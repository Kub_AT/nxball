package com.nxball.game;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 * Player tests.
 * @author Michal
 *
 */
public class PlayerTest {
	
	/**
	 * Test player method.
	 */
	@Test
	public final void testPlayer() {
		Player player = new Player("Player");
		assertEquals("Player", player.getNick());
	}

}
