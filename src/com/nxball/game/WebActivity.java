package com.nxball.game;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.webkit.WebView;
import android.webkit.WebViewClient;

/**
 * Splash Screen Class.
 * 
 * @author KubAT
 * 
 */
public class WebActivity extends Activity {
    /**
     * Splash Delay 3000 = 3sec.
     */
	WebView mWebView;

    /**
     * Called on Create.
     * 
     * @param savedInstanceState
     *            Bundle
     * @see android.app.Activity#onCreate(android.os.Bundle)
     */
    public final void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu);

        mWebView = (WebView) findViewById(R.id.webview);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.loadUrl("http://nxball.tk");
       // mWebView.setWebViewClient(new HelloWebViewClient());
        
    }
    
}