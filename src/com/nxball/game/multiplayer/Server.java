package com.nxball.game.multiplayer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.anddev.andengine.engine.handler.IUpdateHandler;
import org.anddev.andengine.entity.primitive.Rectangle;
import org.anddev.andengine.entity.shape.Shape;
import org.anddev.andengine.entity.sprite.AnimatedSprite;
import org.anddev.andengine.extension.multiplayer.protocol.adt.message.client.IClientMessage;
import org.anddev.andengine.extension.multiplayer.protocol.server.IClientMessageHandler;
import org.anddev.andengine.extension.multiplayer.protocol.server.SocketServer;
import org.anddev.andengine.extension.multiplayer.protocol.server.SocketServer.ISocketServerListener;
import org.anddev.andengine.extension.multiplayer.protocol.server.connector.ClientConnector;
import org.anddev.andengine.extension.multiplayer.protocol.server.connector.SocketConnectionClientConnector;
import org.anddev.andengine.extension.multiplayer.protocol.server.connector.SocketConnectionClientConnector.ISocketConnectionClientConnectorListener;
import org.anddev.andengine.extension.multiplayer.protocol.shared.SocketConnection;
import org.anddev.andengine.extension.physics.box2d.FixedStepPhysicsWorld;
import org.anddev.andengine.extension.physics.box2d.PhysicsFactory;
import org.anddev.andengine.extension.physics.box2d.PhysicsWorld;
import org.anddev.andengine.extension.physics.box2d.util.Vector2Pool;
import org.anddev.andengine.extension.physics.box2d.util.constants.PhysicsConstants;
import org.anddev.andengine.util.Debug;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.nxball.game.C;
import com.nxball.game.multiplayer.messages.BallChangeServerMessage;
import com.nxball.game.multiplayer.messages.ConnectionCloseServerMessage;
import com.nxball.game.multiplayer.messages.ControlClientMessage;
import com.nxball.game.multiplayer.messages.GamestateChangeServerMessage;
import com.nxball.game.multiplayer.messages.KickClientMessage;
import com.nxball.game.multiplayer.messages.MessageFlags;
import com.nxball.game.multiplayer.messages.PlayerDataClientMessage;
import com.nxball.game.multiplayer.messages.PlayersDataServerMessage;
import com.nxball.game.multiplayer.messages.ScoreChangeServerMessage;

/**
 * Server Class.
 * 
 */
public class Server implements C, MessageFlags, IUpdateHandler {

    private Map<ClientConnector<SocketConnection>, Vector2> players;
    private Vector2 whereIsBall, whereIsBall2;
    private SocketServer<SocketConnectionClientConnector> mSocketServer;
    private MultiplayerActivity multiplayerActivity;
    private PhysicsWorld mPhysicsWorld;
    private final float ballLinearDamping = 0.7f;
    private final float playerLinearDamping = 6f;
    public static final short CATEGORYBIT_WALL = 1;
    public static final short CATEGORYBIT_WALL2 = 2;
    public static final short CATEGORYBIT_FACE = 4;
    public static final short CATEGORYBIT_SOCCER = 8;
    public int numberOfPlayers = 0, beginGame =0;
    public int licznik = -1, licznik2 = 0, korekcja = 0;
    private FixtureDef carFixtureDef;
    private FixtureDef carFixtureDef2;
    private FixtureDef carFixtureDef3;
    private FixtureDef carFixtureDef4;
    private Map<ClientConnector<SocketConnection>, Body> bodies;
    private Body Ball;
    public AnimatedSprite face;
    public float kontrolka = 0, kontrolka2 = 0;
    private ClientConnector<SocketConnection> kliencik = null;
    private Vector2 velocit = null;
    float wynikLewo = 0, wynikPrawo = 0;
    ArrayList<String> playerImei;
    Boolean imeisSent = false;

    public static final short MASKBITS_WALL = CATEGORYBIT_WALL
            + CATEGORYBIT_FACE + CATEGORYBIT_SOCCER;
    public static final short MASKBITS_WALL2 = CATEGORYBIT_WALL2
            + CATEGORYBIT_SOCCER;
    public static final short MASKBITS_FACE = CATEGORYBIT_WALL
            + CATEGORYBIT_FACE + CATEGORYBIT_SOCCER;
    public static final short MASKBITS_SOCCER = CATEGORYBIT_WALL
            + CATEGORYBIT_WALL2 + CATEGORYBIT_FACE;

    /**
     * Server Method.
     * @param port
     * @param multiplayerActivity
     */
    public Server(int port, MultiplayerActivity multiplayerActivity) {
    	
    	playerImei = new ArrayList<String>();
        this.players = new HashMap<ClientConnector<SocketConnection>, Vector2>();
        Debug.i("players");
        this.whereIsBall = new Vector2();
        this.whereIsBall2 = new Vector2(0, 0);
        this.bodies = new HashMap<ClientConnector<SocketConnection>, Body>();
        Debug.i("bodies");
        this.multiplayerActivity = multiplayerActivity;
        this.mPhysicsWorld = new FixedStepPhysicsWorld(20, new Vector2(0, 0),
                true, 4, 1);
        // this.mPhysicsWorld.setContactListener((ContactListener) this);
        carFixtureDef = PhysicsFactory.createFixtureDef(1, 0.5f, 0.5f, false,
                CATEGORYBIT_WALL, MASKBITS_WALL, (short) 0);
        carFixtureDef2 = PhysicsFactory.createFixtureDef(1, 0.5f, 0.5f, false,
                CATEGORYBIT_WALL2, MASKBITS_WALL2, (short) 0);
        carFixtureDef3 = PhysicsFactory.createFixtureDef(25, 0.5f, 0.5f, false,
                CATEGORYBIT_FACE, MASKBITS_FACE, (short) 0);
        carFixtureDef4 = PhysicsFactory.createFixtureDef(25, 0.5f, 0.5f, false,
                CATEGORYBIT_SOCCER, MASKBITS_SOCCER, (short) 0);

        final Shape ground = new Rectangle(0, CAMERA_HEIGHT - 2, CAMERA_WIDTH,
                2);
        final Shape roof = new Rectangle(0, 0, CAMERA_WIDTH, 2);
        final Shape left = new Rectangle(0, 0, 2, CAMERA_HEIGHT);
        final Shape right = new Rectangle(CAMERA_WIDTH - 2, 0, 2, CAMERA_HEIGHT);

        final Shape slupek = new Rectangle(
                CAMERA_WIDTH
                        / 2
                        - (multiplayerActivity.mBoiskoTextureRegion.getWidth() / 2)
                        - multiplayerActivity.mBoiskoTextureRegion.getWidth(),
                (float) (0 + multiplayerActivity.mPlayerTextureRegion
                        .getWidth() + 0.30 * multiplayerActivity.mBoiskoTextureRegion
                        .getHeight()), 2, 2);
        final Shape slupek2 = new Rectangle(
                CAMERA_WIDTH
                        / 2
                        + (multiplayerActivity.mBoiskoTextureRegion.getWidth() / 2)
                        + multiplayerActivity.mBoiskoTextureRegion.getWidth()
                        - 2,
                (float) (0 + multiplayerActivity.mPlayerTextureRegion
                        .getWidth() + 0.30 * multiplayerActivity.mBoiskoTextureRegion
                        .getHeight()), 2, 2);
        final Shape slupek3 = new Rectangle(
                CAMERA_WIDTH
                        / 2
                        - (multiplayerActivity.mBoiskoTextureRegion.getWidth() / 2)
                        - multiplayerActivity.mBoiskoTextureRegion.getWidth(),
                (float) (0 + multiplayerActivity.mPlayerTextureRegion
                        .getWidth() + 0.6914 * multiplayerActivity.mBoiskoTextureRegion
                        .getHeight()), 2, 2);
        final Shape slupek4 = new Rectangle(
                CAMERA_WIDTH
                        / 2
                        + (multiplayerActivity.mBoiskoTextureRegion.getWidth() / 2)
                        + multiplayerActivity.mBoiskoTextureRegion.getWidth()
                        - 2,
                (float) (0 + multiplayerActivity.mPlayerTextureRegion
                        .getWidth() + 0.6914 * multiplayerActivity.mBoiskoTextureRegion
                        .getHeight()), 2, 2);

        final Shape groundB = new Rectangle(CAMERA_WIDTH / 2
                - (multiplayerActivity.mBoiskoTextureRegion.getWidth() / 2)
                - multiplayerActivity.mBoiskoTextureRegion.getWidth(), 0
                + multiplayerActivity.mPlayerTextureRegion.getWidth()
                + multiplayerActivity.mBoiskoTextureRegion.getHeight(),
                multiplayerActivity.mBoiskoTextureRegion.getWidth() * 3, 1);
        final Shape roofB = new Rectangle(CAMERA_WIDTH / 2
                - (multiplayerActivity.mBoiskoTextureRegion.getWidth() / 2)
                - multiplayerActivity.mBoiskoTextureRegion.getWidth(),
                0 + multiplayerActivity.mPlayerTextureRegion.getWidth(),
                multiplayerActivity.mBoiskoTextureRegion.getWidth() * 3, 1);
        final Shape leftB1 = new Rectangle(CAMERA_WIDTH / 2
                - (multiplayerActivity.mBoiskoTextureRegion.getWidth() / 2)
                - multiplayerActivity.mBoiskoTextureRegion.getWidth(),
                0 + multiplayerActivity.mPlayerTextureRegion.getWidth(), 1,
                (float) (0.30 * multiplayerActivity.mBoiskoTextureRegion
                        .getHeight()));
        final Shape rightB1 = new Rectangle(CAMERA_WIDTH / 2
                + (multiplayerActivity.mBoiskoTextureRegion.getWidth() / 2)
                + multiplayerActivity.mBoiskoTextureRegion.getWidth(),
                0 + multiplayerActivity.mPlayerTextureRegion.getWidth(), 1,
                (float) (0.30 * multiplayerActivity.mBoiskoTextureRegion
                        .getHeight()));
        final Shape leftB2 = new Rectangle(
                CAMERA_WIDTH
                        / 2
                        - (multiplayerActivity.mBoiskoTextureRegion.getWidth() / 2)
                        - multiplayerActivity.mBoiskoTextureRegion.getWidth(),
                (float) (0 + multiplayerActivity.mPlayerTextureRegion
                        .getWidth() + 0.6914 * multiplayerActivity.mBoiskoTextureRegion
                        .getHeight()), 1,
                (float) (0.30 * multiplayerActivity.mBoiskoTextureRegion
                        .getHeight()));
        final Shape rightB2 = new Rectangle(
                CAMERA_WIDTH
                        / 2
                        + (multiplayerActivity.mBoiskoTextureRegion.getWidth() / 2)
                        + multiplayerActivity.mBoiskoTextureRegion.getWidth(),
                (float) (0 + multiplayerActivity.mPlayerTextureRegion
                        .getWidth() + 0.6914 * multiplayerActivity.mBoiskoTextureRegion
                        .getHeight()), 1,
                (float) (0.30 * multiplayerActivity.mBoiskoTextureRegion
                        .getHeight()));

        final Shape goalLeft = new Rectangle(
                CAMERA_WIDTH
                        / 2
                        - (multiplayerActivity.mBoiskoTextureRegion.getWidth() / 2)
                        - multiplayerActivity.mBoiskoTextureRegion.getWidth()
                        - 16,
                (float) (0 + multiplayerActivity.mPlayerTextureRegion
                        .getWidth() + (0.30 * multiplayerActivity.mBoiskoTextureRegion
                        .getHeight())), 1,
                (float) (0.40 * multiplayerActivity.mBoiskoTextureRegion
                        .getHeight()));
        final Shape goalLeftUp = new Rectangle(
                CAMERA_WIDTH
                        / 2
                        - (multiplayerActivity.mBoiskoTextureRegion.getWidth() / 2)
                        - multiplayerActivity.mBoiskoTextureRegion.getWidth()
                        - 16,
                (float) (0 + multiplayerActivity.mPlayerTextureRegion
                        .getWidth() + (0.30 * multiplayerActivity.mBoiskoTextureRegion
                        .getHeight())), 16, 1);
        final Shape goalLeftDown = new Rectangle(
                CAMERA_WIDTH
                        / 2
                        - (multiplayerActivity.mBoiskoTextureRegion.getWidth() / 2)
                        - multiplayerActivity.mBoiskoTextureRegion.getWidth()
                        - 16,
                (float) (0 + multiplayerActivity.mPlayerTextureRegion
                        .getWidth() + 0.6914 * multiplayerActivity.mBoiskoTextureRegion
                        .getHeight()), 16, 1);

        final Shape goalRight = new Rectangle(
                CAMERA_WIDTH
                        / 2
                        + (multiplayerActivity.mBoiskoTextureRegion.getWidth() / 2)
                        + multiplayerActivity.mBoiskoTextureRegion.getWidth()
                        + 16,
                (float) (0 + multiplayerActivity.mPlayerTextureRegion
                        .getWidth() + (0.33 * multiplayerActivity.mBoiskoTextureRegion
                        .getHeight())), 1,
                (float) (0.40 * multiplayerActivity.mBoiskoTextureRegion
                        .getHeight()));
        final Shape goalRightUp = new Rectangle(
                CAMERA_WIDTH
                        / 2
                        + (multiplayerActivity.mBoiskoTextureRegion.getWidth() / 2)
                        + multiplayerActivity.mBoiskoTextureRegion.getWidth(),
                (float) (0 + multiplayerActivity.mPlayerTextureRegion
                        .getWidth() + (0.30 * multiplayerActivity.mBoiskoTextureRegion
                        .getHeight())), 16, 1);
        final Shape goalRightDown = new Rectangle(
                CAMERA_WIDTH
                        / 2
                        + (multiplayerActivity.mBoiskoTextureRegion.getWidth() / 2)
                        + multiplayerActivity.mBoiskoTextureRegion.getWidth(),
                (float) (0 + multiplayerActivity.mPlayerTextureRegion
                        .getWidth() + 0.6914 * multiplayerActivity.mBoiskoTextureRegion
                        .getHeight()), 16, 1);

        Ball = PhysicsFactory.createCircleBody(this.mPhysicsWorld,
                new Rectangle(100, 100, 16, 16), BodyType.DynamicBody,
                carFixtureDef4);
        Ball.setLinearDamping(this.ballLinearDamping);
        Ball.setAngularDamping(this.ballLinearDamping);

        PhysicsFactory.createBoxBody(this.mPhysicsWorld, ground,
                BodyType.StaticBody, carFixtureDef);
        PhysicsFactory.createBoxBody(this.mPhysicsWorld, roof,
                BodyType.StaticBody, carFixtureDef);
        PhysicsFactory.createBoxBody(this.mPhysicsWorld, left,
                BodyType.StaticBody, carFixtureDef);
        PhysicsFactory.createBoxBody(this.mPhysicsWorld, right,
                BodyType.StaticBody, carFixtureDef);

        PhysicsFactory.createBoxBody(this.mPhysicsWorld, slupek,
                BodyType.StaticBody, carFixtureDef);
        PhysicsFactory.createBoxBody(this.mPhysicsWorld, slupek2,
                BodyType.StaticBody, carFixtureDef);
        PhysicsFactory.createBoxBody(this.mPhysicsWorld, slupek3,
                BodyType.StaticBody, carFixtureDef);
        PhysicsFactory.createBoxBody(this.mPhysicsWorld, slupek4,
                BodyType.StaticBody, carFixtureDef);

        PhysicsFactory.createBoxBody(this.mPhysicsWorld, groundB,
                BodyType.StaticBody, carFixtureDef2);
        PhysicsFactory.createBoxBody(this.mPhysicsWorld, roofB,
                BodyType.StaticBody, carFixtureDef2);
        PhysicsFactory.createBoxBody(this.mPhysicsWorld, leftB1,
                BodyType.StaticBody, carFixtureDef2);
        PhysicsFactory.createBoxBody(this.mPhysicsWorld, rightB1,
                BodyType.StaticBody, carFixtureDef2);
        PhysicsFactory.createBoxBody(this.mPhysicsWorld, leftB2,
                BodyType.StaticBody, carFixtureDef2);
        PhysicsFactory.createBoxBody(this.mPhysicsWorld, rightB2,
                BodyType.StaticBody, carFixtureDef2);

        PhysicsFactory.createBoxBody(this.mPhysicsWorld, goalLeft,
                BodyType.StaticBody, carFixtureDef2);
        PhysicsFactory.createBoxBody(this.mPhysicsWorld, goalRight,
                BodyType.StaticBody, carFixtureDef2);

        PhysicsFactory.createBoxBody(this.mPhysicsWorld, goalLeftUp,
                BodyType.StaticBody, carFixtureDef2);
        PhysicsFactory.createBoxBody(this.mPhysicsWorld, goalRightUp,
                BodyType.StaticBody, carFixtureDef2);

        PhysicsFactory.createBoxBody(this.mPhysicsWorld, goalLeftDown,
                BodyType.StaticBody, carFixtureDef2);
        PhysicsFactory.createBoxBody(this.mPhysicsWorld, goalRightDown,
                BodyType.StaticBody, carFixtureDef2);

        this.mSocketServer = new SocketServer<SocketConnectionClientConnector>(
                port, new ClientConnectorListener(),
                new ExampleServerStateListener()) {
            @Override
            protected SocketConnectionClientConnector newClientConnector(
                    final SocketConnection pSocketConnection)
                    throws IOException {
                return new SocketConnectionClientConnector(pSocketConnection);
            }
        };

    }

    /**
     * start.
     */
    public void start() {
        mSocketServer.start();
    }

    /**
     * finish.
     */
    public void finish() {
        terminate();
    }

    /**
     * terminate.
     */
    public void terminate() {
        try {
            this.mSocketServer
                    .sendBroadcastServerMessage(new ConnectionCloseServerMessage());
        } catch (final IOException e) {
            Debug.e(e);
        }
        this.mSocketServer.terminate();
    }

    /**
     * sendGameChanged.
     */
    public void sendGameChanged() {
        try {
            if (players.values() == null) {
                Debug.e("players is null");
            } else {
                mSocketServer
                        .sendBroadcastServerMessage(new GamestateChangeServerMessage(
                                players.values()));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * sendGameChanged.
     * @param client
     */
    public void sendGameChanged(ClientConnector<SocketConnection> client) {
        try {
            if (players.values() == null) {
                Debug.e("players is null");
            } else {
                client.sendServerMessage(new GamestateChangeServerMessage(
                        players.values()));

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * sendBallChanged.
     */
    public void sendBallChanged() {
        try {

            mSocketServer
                    .sendBroadcastServerMessage(new BallChangeServerMessage(
                            whereIsBall));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * sendScoreChanged.
     */
    public void sendScoreChanged() {
        try {

            mSocketServer
                    .sendBroadcastServerMessage(new ScoreChangeServerMessage(
                            wynikLewo, wynikPrawo));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * gameControled
     * This method is called when client sends ControlClientMessage
     * @param client
     * @param control
     * @param control2
     */
    protected void gameControled(ClientConnector<SocketConnection> client,
            float control, float control2) {

        sendGameChanged();
        // Vector2 playerPos = players.get(client);
        kliencik = client;
        kontrolka = control;
        kontrolka2 = control2;
        Debug.i("w game controlede x " + control + " y " + control2);

    }

    /**
     * kickBall.
     * This method is called when client sends KickClientMessage
     * @param client
     */
    protected void kickBall(ClientConnector<SocketConnection> client) {
        if (distance(players.get(client), whereIsBall) <= 33.0f) {
            // float velX = Ball.getLinearVelocity().x;
            // float velY = Ball.getLinearVelocity().y;
            Ball.applyLinearImpulse(
                    new Vector2((whereIsBall.x - players.get(client).x) / 2,
                            (whereIsBall.y - players.get(client).y) / 2), Ball
                            .getWorldCenter());
            Debug.i("kickin ball!");
        } else {
            Debug.i("za daleko!");
        }
    }

    /**
     * Distance between two objects
     * @param pos1
     * @param pos2
     * @return
     */
    protected float distance(Vector2 pos1, Vector2 pos2) {
        float x1 = pos1.x + 16;
        float x2 = pos2.x + 8;
        float y1 = pos1.y + 16;
        float y2 = pos2.y + 8;
        return (float) Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
    }

    /**
     * onUpdate
     * @param pSecondsElapsed
     */
    @Override
    public void onUpdate(final float pSecondsElapsed) {
        //Debug.i("playerImei size = " + playerImei.size());
        //Debug.i("players size = " + players.size());
        //Debug.i("w onUpdate  jestem");
        if (playerImei.size() == players.size() && imeisSent == false) {
            try {

                mSocketServer
                        .sendBroadcastServerMessage(new PlayersDataServerMessage(
                                playerImei));
                imeisSent = true;
            } catch (IOException e) {
            	Debug.i("wysylam wszystkie imei - cos nie poszlo");
                e.printStackTrace();
            }
        }
        if (kliencik != null) {
            //Debug.i("w onUpdate  jestem w if");
            // Vector2 playerPos2 = players.get(kliencik);
            velocit = Vector2Pool.obtain(kontrolka, kontrolka2);
            //Debug.i("w onUpdate x " + kontrolka + " y " + kontrolka2);
            bodies.get(kliencik).setLinearVelocity(velocit);
            Vector2Pool.recycle(velocit);

        }

        Iterator itP = players.entrySet().iterator();
        Iterator itB = bodies.entrySet().iterator();
        while (itP.hasNext() || itB.hasNext()) {
            Map.Entry pairsP = (Map.Entry) itP.next();
            Map.Entry pairsB = (Map.Entry) itB.next();
            ClientConnector<SocketConnection> pppK = (ClientConnector<SocketConnection>) pairsP
                    .getKey();
            ClientConnector<SocketConnection> bbbK = (ClientConnector<SocketConnection>) pairsB
                    .getKey();
            Vector2 pppB = (Vector2) pairsP.getValue();
            Body bbbB = (Body) pairsB.getValue();
            // System.out.println(pairs.getKey() + " = " + pairs.getValue());
            pppB.x = bbbB.getPosition().x
                    * PhysicsConstants.PIXEL_TO_METER_RATIO_DEFAULT - 16;
            pppB.y = bbbB.getPosition().y
                    * PhysicsConstants.PIXEL_TO_METER_RATIO_DEFAULT - 16;

        }

        whereIsBall.x = Ball.getPosition().x
                * PhysicsConstants.PIXEL_TO_METER_RATIO_DEFAULT - 8;
        whereIsBall.y = Ball.getPosition().y
                * PhysicsConstants.PIXEL_TO_METER_RATIO_DEFAULT - 8;

        // Dodawanie punkt�w
        if (whereIsBall.x < (CAMERA_WIDTH / 2
                - (multiplayerActivity.mBoiskoTextureRegion.getWidth() / 2)
                - multiplayerActivity.mBoiskoTextureRegion.getWidth() - 10) || (beginGame==0 && bodies.size()==2))
                {
        	if ((whereIsBall.x < (CAMERA_WIDTH / 2
                    - (multiplayerActivity.mBoiskoTextureRegion.getWidth() / 2)
                    - multiplayerActivity.mBoiskoTextureRegion.getWidth() - 10)))
        		wynikLewo += 1;
            // Iterator itP2 = players.entrySet().iterator();
            Ball.setTransform((CAMERA_WIDTH / 2 + 20) / 32,
                    (CAMERA_HEIGHT / 2 + 8) / 32, Ball.getAngle());
            Ball.applyLinearImpulse(new Vector2(1, 0), Ball.getWorldCenter());
            Ball.setLinearVelocity(new Vector2(0, 0));
            Iterator itBB = bodies.entrySet().iterator();
            while (itBB.hasNext()){
           	    Map.Entry pairsPP = (Map.Entry) itBB.next();
                Body pppK = (Body) pairsPP.getValue();
                if (licznik2==0)
                {
                	pppK.setTransform((CAMERA_WIDTH / 2 - 60) / 32 ,
                			(CAMERA_HEIGHT / 2 + 8) / 32, Ball.getAngle());
                	
                }
                if (licznik2==1)
               	{
                	pppK.setTransform((CAMERA_WIDTH / 2 + 128) / 32 ,
                			(CAMERA_HEIGHT / 2 + 8) / 32, Ball.getAngle());
               	}
                licznik2 += licznik2 +1;
                if (licznik2==2)
                	licznik2=0;
                
            }
            licznik2=0;
            sendGameChanged();
            
            sendScoreChanged();
            if (beginGame==0)
            	beginGame=1;
        }

        if (whereIsBall.x > (CAMERA_WIDTH / 2
                + (multiplayerActivity.mBoiskoTextureRegion.getWidth() / 2)
                + multiplayerActivity.mBoiskoTextureRegion.getWidth() - 6)) {
            wynikPrawo += 1;
            // Iterator itP2 = players.entrySet().iterator();
            Ball.setTransform((CAMERA_WIDTH / 2 + 20) / 32 + 6 / 10,
                    (CAMERA_HEIGHT / 2 + 8) / 32, Ball.getAngle());
            Ball.setLinearVelocity(new Vector2(0, 0));
            Iterator itBB = bodies.entrySet().iterator();
            while (itBB.hasNext()){
           	    Map.Entry pairsPP = (Map.Entry) itBB.next();
                Body pppK = (Body) pairsPP.getValue();
                if (licznik2==0)
                {
                	pppK.setTransform((CAMERA_WIDTH / 2 - 128) / 32 ,
                            (CAMERA_HEIGHT / 2 + 8) / 32, Ball.getAngle());
                }
                if (licznik2==1)
               	{
                	pppK.setTransform((CAMERA_WIDTH / 2 + 60) / 32 ,
                            (CAMERA_HEIGHT / 2 + 8) / 32, Ball.getAngle());
               	}

                licznik2 += licznik2 +1;
                if (licznik2==2)
                	licznik2=0;
                
            }
            licznik2=0;
            sendGameChanged();
            sendScoreChanged();
        }
        kontrolka = 0;
        kontrolka2 = 0;
        licznik = 1;
        korekcja += 1;
        this.mPhysicsWorld.onUpdate(pSecondsElapsed);
        if (whereIsBall != whereIsBall2)
            sendBallChanged();
        whereIsBall2.x = whereIsBall.x;
        whereIsBall2.y = whereIsBall.y;
        kliencik = null;
    }

    /**
     * ClientConnectorListener.
     * Listening for client connections
     *
     */
    private class ClientConnectorListener implements
            ISocketConnectionClientConnectorListener {

        @Override
        public void onStarted(final ClientConnector<SocketConnection> pConnector) {

            multiplayerActivity.toast("SERVER: Client connected: "
                    + pConnector.getConnection().getSocket().getInetAddress()
                            .getHostAddress());
            

            
            // register listening for client message
            pConnector.registerClientMessage(FLAG_MESSAGE_CLIENT_CONTROL,
                    ControlClientMessage.class,
                    new IClientMessageHandler<SocketConnection>() {
                        @Override
                        public void onHandleMessage(
                                ClientConnector<SocketConnection> client,
                                IClientMessage arg1) throws IOException {
                            ControlClientMessage ccm = (ControlClientMessage) arg1;
                           // Debug.i("controlled!");
                            gameControled(client, ccm.getMove2(),
                                    ccm.getMove3());

                        }
                    });
            // register listening for client kick message
            pConnector.registerClientMessage(FLAG_MESSAGE_CLIENT_KICK,
                    KickClientMessage.class,
                    new IClientMessageHandler<SocketConnection>() {
                        @Override
                        public void onHandleMessage(
                                ClientConnector<SocketConnection> client,
                                IClientMessage arg1) throws IOException {
                            KickClientMessage ccm = (KickClientMessage) arg1;
                            //Debug.i("kick!!");
                            kickBall(client);
                        }
                    });
            

            // register listening for client kick message
            pConnector.registerClientMessage(FLAG_MESSAGE_CLIENT_PLAYER_DATA,
                    PlayerDataClientMessage.class,
                    new IClientMessageHandler<SocketConnection>() {
                        @Override
                        public void onHandleMessage(
                                ClientConnector<SocketConnection> client,
                                IClientMessage arg1) throws IOException {
                            PlayerDataClientMessage ccm = (PlayerDataClientMessage) arg1;
                            Debug.i("player imei received");
                            Debug.i("imei " + ccm.getIMEI());
                            playerImei.add(ccm.getIMEI());
                        }
                    });

            // add new player
            final int centerX = 0;
            final int centerY = 0;
            players.put(pConnector, new Vector2(CAMERA_WIDTH/2 + 128 * licznik,
            		CAMERA_HEIGHT/2 - 16));
            Ball.setTransform((CAMERA_WIDTH / 2 + 15) / 32 + 6 / 10,
                    (CAMERA_HEIGHT / 2 + 8) / 32, Ball.getAngle());
            bodies.put(pConnector, PhysicsFactory.createCircleBody(
                    mPhysicsWorld, new Rectangle(CAMERA_WIDTH/2 + 128 * licznik,
                    		CAMERA_HEIGHT/2 - 16 , 32, 32), BodyType.DynamicBody,
                    carFixtureDef3));
            // add linear damping for players
            bodies.get(pConnector).setLinearDamping(playerLinearDamping);
            bodies.get(pConnector).setAngularDamping(playerLinearDamping);

            licznik = licznik + 2;

            sendGameChanged(); // send info to all previous connected clients
            sendGameChanged(pConnector); // send to newly connected client
        }

        @Override
        public void onTerminated(
                final ClientConnector<SocketConnection> pConnector) {
            multiplayerActivity.toast("SERVER: Client disconnected: "
                    + pConnector.getConnection().getSocket().getInetAddress()
                            .getHostAddress());

            players.remove(pConnector);
            sendGameChanged();
        }
    }

    /**
     * ExampleServerStateListener
     * Listening for server state changes
     *
     */
    private class ExampleServerStateListener implements
            ISocketServerListener<SocketConnectionClientConnector> {
        @Override
        public void onStarted(
                final SocketServer<SocketConnectionClientConnector> pSocketServer) {
            multiplayerActivity.toast("SERVER: Started.");
        }

        @Override
        public void onTerminated(
                final SocketServer<SocketConnectionClientConnector> pSocketServer) {
            multiplayerActivity.toast("SERVER: Terminated.");
        }

        @Override
        public void onException(
                final SocketServer<SocketConnectionClientConnector> pSocketServer,
                final Throwable pThrowable) {
            Debug.e(pThrowable);
            multiplayerActivity.toast("SERVER: Exception: " + pThrowable);
        }
    }

    /**
     * reset.
     */
    @Override
    public void reset() {
        // TODO Auto-generated method stub

    }

}
