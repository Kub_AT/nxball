﻿package com.nxball.game.multiplayer;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.microedition.khronos.opengles.GL10;

import org.anddev.andengine.engine.Engine;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.engine.camera.hud.controls.AnalogOnScreenControl;
import org.anddev.andengine.engine.camera.hud.controls.AnalogOnScreenControl.IAnalogOnScreenControlListener;
import org.anddev.andengine.engine.camera.hud.controls.BaseOnScreenControl;
import org.anddev.andengine.engine.handler.IUpdateHandler;
import org.anddev.andengine.engine.handler.timer.ITimerCallback;
import org.anddev.andengine.engine.handler.timer.TimerHandler;
import org.anddev.andengine.engine.options.EngineOptions;
import org.anddev.andengine.engine.options.EngineOptions.ScreenOrientation;
import org.anddev.andengine.engine.options.resolutionpolicy.RatioResolutionPolicy;
import org.anddev.andengine.entity.primitive.Rectangle;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.entity.scene.background.ColorBackground;
import org.anddev.andengine.entity.shape.Shape;
import org.anddev.andengine.entity.sprite.AnimatedSprite;
import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.entity.text.ChangeableText;
import org.anddev.andengine.entity.util.FPSLogger;
import org.anddev.andengine.extension.input.touch.controller.MultiTouch;
import org.anddev.andengine.extension.input.touch.controller.MultiTouchController;
import org.anddev.andengine.extension.input.touch.exception.MultiTouchException;
import org.anddev.andengine.extension.multiplayer.protocol.adt.message.server.IServerMessage;
import org.anddev.andengine.extension.multiplayer.protocol.client.IServerMessageHandler;
import org.anddev.andengine.extension.multiplayer.protocol.client.connector.ServerConnector;
import org.anddev.andengine.extension.multiplayer.protocol.client.connector.SocketConnectionServerConnector;
import org.anddev.andengine.extension.multiplayer.protocol.client.connector.SocketConnectionServerConnector.ISocketConnectionServerConnectorListener;
import org.anddev.andengine.extension.multiplayer.protocol.shared.SocketConnection;
import org.anddev.andengine.extension.multiplayer.protocol.util.WifiUtils;
import org.anddev.andengine.input.touch.TouchEvent;
import org.anddev.andengine.opengl.font.Font;
import org.anddev.andengine.opengl.texture.TextureOptions;
import org.anddev.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.anddev.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.opengl.texture.region.TiledTextureRegion;
import org.anddev.andengine.ui.activity.BaseGameActivity;
import org.anddev.andengine.util.Debug;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.PowerManager;
import android.telephony.TelephonyManager;
import android.widget.EditText;
import android.widget.Toast;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.nxball.game.AfterMatchActivity;
import com.nxball.game.C;
import com.nxball.game.multiplayer.messages.BallChangeServerMessage;
import com.nxball.game.multiplayer.messages.ConnectionCloseServerMessage;
import com.nxball.game.multiplayer.messages.ControlClientMessage;
import com.nxball.game.multiplayer.messages.GamestateChangeServerMessage;
import com.nxball.game.multiplayer.messages.KickClientMessage;
import com.nxball.game.multiplayer.messages.MessageFlags;
import com.nxball.game.multiplayer.messages.PlayerDataClientMessage;
import com.nxball.game.multiplayer.messages.PlayersDataServerMessage;
import com.nxball.game.multiplayer.messages.ScoreChangeServerMessage;

/**
 * MultiplayerActivity class.
 * 
 */
public class MultiplayerActivity extends BaseGameActivity implements C,
        MessageFlags {

    /*
     * Constants
     */
    // Multiplayer constants

    public static final int GAME_WIDTH = 480;
    public static final int GAME_WIDTH_HALF = GAME_WIDTH / 2;
    public static final int GAME_HEIGHT = 320;
    public static final int GAME_HEIGHT_HALF = GAME_HEIGHT / 2;

    public static final int PADDLE_WIDTH = 80;
    public static final int PADDLE_WIDTH_HALF = PADDLE_WIDTH / 2;
    public static final int PADDLE_HEIGHT = 20;
    public static final int PADDLE_HEIGHT_HALF = PADDLE_HEIGHT / 2;

    /**
     * CAMERA_WIDTH - camerta width.
     */
    private static final int CAMERA_WIDTH = 480;

    /**
     * CAMERA_HEIGHT - camerta height.
     */
    private static final int CAMERA_HEIGHT = 320;

    /**
     * Kamera.
     */

    /**
     * Bitmap Textur.
     */
    private BitmapTextureAtlas mBitmapTextureAtlas;
    private BitmapTextureAtlas mBoiskoTextureAtlas;

    /**
     * Face Texture Region.
     */
    private TiledTextureRegion mSoccerTextureRegion;
    private BitmapTextureAtlas mSoccerTextureAtlas;
    private TextureRegion mSlupekTextureRegion;
    private BitmapTextureAtlas mSlupekTextureAtlas;
    TextureRegion mBoiskoTextureRegion;
    private BitmapTextureAtlas mBoisko2TextureAtlas;
    private TextureRegion mBoisko2TextureRegion;
    private BitmapTextureAtlas mBoisko3TextureAtlas;
    private TextureRegion mBoisko3TextureRegion;
    private BitmapTextureAtlas mTeamATextureAtlas;
    private TextureRegion mTeamATextureRegion;
    private BitmapTextureAtlas mTeamBTextureAtlas;
    private TextureRegion mTeamBTextureRegion;
    TiledTextureRegion mPlayerTextureRegion;
	TiledTextureRegion mPlayerTextureRegion2;
    /**
     * OnScreenControlTexture.
     */
    private BitmapTextureAtlas mOnScreenControlTexture;

    /**
     * OnScreenControlBaseTextureRegion.
     */
    private TextureRegion mOnScreenControlBaseTextureRegion;

    /**
     * OnScreenControlKnobTextureRegion.
     */
    private TextureRegion mOnScreenControlKnobTextureRegion;

    /**
     * PlaceOnScreenControlsAtDifferentVerticalLocations.
     */
    private boolean mPlaceOnScreenControlsAtDifferentVerticalLocations = false;

    /**
     * OnScreenControlKickTexture.
     */
    private BitmapTextureAtlas mOnScreenControlKickTexture;

    /**
     * OnScreenControlKickTextureRegion.
     */
    private TextureRegion mOnScreenControlKickTextureRegion;
    public Body mCarBody3;
    int wynikLewo = 0, wynikPrawo = 0;
    int positionCheck=0;
    /*
     * pomiar czasu
     */
    private BitmapTextureAtlas mFontTexture;
    private Font mFont;

    private int seconds = 0;
    private int minutes = 0;

    /*
     * wynik
     */
    private BitmapTextureAtlas sFontTexture;
    private Font sFont;
    
    private BitmapTextureAtlas tFontTexture;
    private Font tFont;

    private float team_a_score = 0;
    private float team_b_score = 0;

    int licznik = 0, licznik2 = 0, czyJestPilka = 0;

    private static final String LOCALHOST_IP = "127.0.0.1";
    private static final int SERVER_PORT = 4444;

    // Dialogs
    private static final int DIALOG_CHOOSE_SERVER_OR_CLIENT_ID = 0;
    private static final int DIALOG_ENTER_SERVER_IP_ID = DIALOG_CHOOSE_SERVER_OR_CLIENT_ID + 1;
    private static final int DIALOG_SHOW_SERVER_IP_ID = DIALOG_ENTER_SERVER_IP_ID + 1;
    
    private static final int DIALOG_GAMEOVER_ID = 10;

    /*
     * Variables
     */
    private Camera mCamera;
    private Scene scene;

    /*
     * Server - if user is a Host
     */
    private Server server;

    private SharedPreferences sharedPrefs;

    /**
     * Jak szybko gracz sie porusza.
     */
    private final float playerVelocity = 3.0f;

    /*
     * Client Variables
     */
    private List<AnimatedSprite> players;
    private List<AnimatedSprite> balls;
    private ServerConnector<SocketConnection> mServerConnector;
    private PowerManager.WakeLock wl;
    private String serverIP = LOCALHOST_IP;
	private BitmapTextureAtlas mBitmapTextureAtlas2;
	
	private ArrayList<String> playersImeis;

    
    /*
     * method to refill ip adress
     * 
     * 
     */
    
    public String IpAdressShower(String adress){
    	System.out.println(adress);
    	String[] splited = adress.split("\\.");
    	String end = splited[0]+"."+splited[1]+".";
    	return end;
    }
    
    /**
     * onLoadEngine.
     * 
     * 
     */
    @Override
    public final Engine onLoadEngine() {
        this.mCamera = new Camera(0, 0, CAMERA_WIDTH, CAMERA_HEIGHT);
        final Engine engine = new Engine(new EngineOptions(true,
                ScreenOrientation.LANDSCAPE, new RatioResolutionPolicy(
                        CAMERA_WIDTH, CAMERA_HEIGHT), this.mCamera));

        try {
            if (MultiTouch.isSupported(this)) {
                engine.setTouchController(new MultiTouchController());
                if (MultiTouch.isSupportedDistinct(this)) {
                    Toast.makeText(
                            this,
                            "MultiTouch detected --> Both controls will work properly!",
                            Toast.LENGTH_SHORT).show();
                } else {
                    this.mPlaceOnScreenControlsAtDifferentVerticalLocations = true;
                    Toast.makeText(
                            this,
                            "MultiTouch detected, but your device has problems distinguishing between fingers.\n\n"
                                    + "Controls are placed at different vertical locations.",
                            Toast.LENGTH_LONG).show();
                }
            } else {
                Toast.makeText(
                        this,
                        "Sorry your device does NOT support MultiTouch!\n\n(Falling back to SingleTouch.)\n\n"
                                + "Controls are placed at different vertical locations.",
                        Toast.LENGTH_LONG).show();
            }
        } catch (final MultiTouchException e) {
            Toast.makeText(
                    this,
                    "Sorry your Android Version does NOT support MultiTouch!\n\n(Falling back to SingleTouch.)\n\n"
                            + "Controls are placed at different vertical locations.",
                    Toast.LENGTH_LONG).show();
        }

        return engine;
    }

    /**
     * onLoadResources method.
     */
    public final void onLoadResources() {
        BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/");

        // do pomiaru czasu
        this.mFontTexture = new BitmapTextureAtlas(256, 256,
                TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.mFont = new Font(this.mFontTexture, Typeface.create(
                Typeface.DEFAULT, Typeface.BOLD), 24, true, Color.BLACK);
        this.mEngine.getTextureManager().loadTexture(this.mFontTexture);
        this.getFontManager().loadFont(this.mFont);
        //
        // do zliczania punkt�w
        this.sFontTexture = new BitmapTextureAtlas(256, 256,
                TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.sFont = new Font(this.sFontTexture, Typeface.create(
                Typeface.DEFAULT, Typeface.BOLD), 24, true, Color.BLACK);
        this.mEngine.getTextureManager().loadTexture(this.sFontTexture);
        this.getFontManager().loadFont(this.sFont);
        //
        // do wyniku koncowe i zakonczenia
        this.tFontTexture = new BitmapTextureAtlas(256, 256,
                TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.tFont = new Font(this.tFontTexture, Typeface.create(
                Typeface.DEFAULT, Typeface.BOLD), 48, true, Color.BLACK);
        this.mEngine.getTextureManager().loadTexture(this.tFontTexture);
        this.getFontManager().loadFont(this.tFont);
        //
        
        this.mBitmapTextureAtlas = new BitmapTextureAtlas(t32x, t32x,
                TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        
        this.mBitmapTextureAtlas2 = new BitmapTextureAtlas(t32x, t32x,
        		TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.mPlayerTextureRegion = BitmapTextureAtlasTextureRegionFactory
                .createTiledFromAsset(this.mBitmapTextureAtlas, this,
                        "ball.png", 0, 0, 1, 1);
        this.mPlayerTextureRegion2 = BitmapTextureAtlasTextureRegionFactory
                .createTiledFromAsset(this.mBitmapTextureAtlas2, this,
                        "ball2.png", 0, 0, 1, 1);
        



        this.mSoccerTextureAtlas = new BitmapTextureAtlas(t16x, t16x,
                TextureOptions.BILINEAR_PREMULTIPLYALPHA); // mniejsza pilka
        this.mSoccerTextureRegion = BitmapTextureAtlasTextureRegionFactory
                .createTiledFromAsset(this.mSoccerTextureAtlas, this,
                        "soccer2.png", 0, 0, 1, 1);
        
        this.mTeamATextureAtlas = new BitmapTextureAtlas(t16x, t16x,
                TextureOptions.BILINEAR_PREMULTIPLYALPHA); // mniejsza pilka
        this.mTeamATextureRegion = BitmapTextureAtlasTextureRegionFactory
                .createFromAsset(this.mTeamATextureAtlas, this,
                        "teamA.png", 0, 0);
        
        this.mTeamBTextureAtlas = new BitmapTextureAtlas(t16x, t16x,
                TextureOptions.BILINEAR_PREMULTIPLYALPHA); // mniejsza pilka
        this.mTeamBTextureRegion = BitmapTextureAtlasTextureRegionFactory
                .createFromAsset(this.mTeamBTextureAtlas, this,
                        "teamB.png", 0, 0);

        // this.mSoccerTextureAtlas = new BitmapTextureAtlas(t32x, t32x,
        // TextureOptions.BILINEAR_PREMULTIPLYALPHA); // wieksza pilka
        // this.mSoccerTextureRegion =
        // BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mSoccerTextureAtlas,
        // this, "soccer.png", 0, 0, 1, 1);

        this.mBoiskoTextureAtlas = new BitmapTextureAtlas(t128x, t256x,
                TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.mBoiskoTextureRegion = BitmapTextureAtlasTextureRegionFactory
                .createFromAsset(this.mBoiskoTextureAtlas, this, "boisko1.png",
                        0, 0);

        this.mBoisko2TextureAtlas = new BitmapTextureAtlas(t128x, t256x,
                TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.mBoisko2TextureRegion = BitmapTextureAtlasTextureRegionFactory
                .createFromAsset(this.mBoisko2TextureAtlas, this,
                        "boisko2.png", 0, 0);

        this.mBoisko3TextureAtlas = new BitmapTextureAtlas(t128x, t256x,
                TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.mBoisko3TextureRegion = BitmapTextureAtlasTextureRegionFactory
                .createFromAsset(this.mBoisko3TextureAtlas, this,
                        "boisko3.png", 0, 0);

        this.mSlupekTextureAtlas = new BitmapTextureAtlas(t4x, t4x,
                TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.mSlupekTextureRegion = BitmapTextureAtlasTextureRegionFactory
                .createFromAsset(this.mSlupekTextureAtlas, this, "slupek.png",
                        0, 0);

        this.mOnScreenControlTexture = new BitmapTextureAtlas(t256x, t128x,
                TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.mOnScreenControlBaseTextureRegion = BitmapTextureAtlasTextureRegionFactory
                .createFromAsset(this.mOnScreenControlTexture, this,
                        "onscreen_control_base.png", 0, 0);
        this.mOnScreenControlKnobTextureRegion = BitmapTextureAtlasTextureRegionFactory
                .createFromAsset(this.mOnScreenControlTexture, this,
                        "onscreen_control_knob.png", t128x, 0);

        this.mOnScreenControlKickTexture = new BitmapTextureAtlas(t128x, t64x,
                TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.mOnScreenControlKickTextureRegion = BitmapTextureAtlasTextureRegionFactory
                .createFromAsset(this.mOnScreenControlKickTexture, this,
                        "onscreen_control_kick.png", 0, 0);

        this.mEngine.getTextureManager().loadTextures(this.mBitmapTextureAtlas,
                this.mOnScreenControlTexture, this.mOnScreenControlKickTexture,
                this.mBoiskoTextureAtlas, this.mBoisko2TextureAtlas,
                this.mBoisko3TextureAtlas, this.mSoccerTextureAtlas,
                this.mBitmapTextureAtlas2, this.mTeamATextureAtlas,
                this.mTeamBTextureAtlas); // this.mBackgroundTexture

    }

    /**
     * onLoadScene method.
     * 
     * @return scene
     */
    public final Scene onLoadScene() {
        /**
         * RGB
         */

        this.mEngine.registerUpdateHandler(new FPSLogger());

        final float backgroundRed = 0.572f;
        final float backgroundGreen = 0.749f;
        final float backgroundBlue = 0.4156f;

        /**
         * Alpha.
         */
        final float alpha = 0.5f;

        this.mEngine.registerUpdateHandler(new FPSLogger());

        this.scene = new Scene();
        scene.setBackground(new ColorBackground(backgroundRed, backgroundGreen,
                backgroundBlue));

        final int centerX = (CAMERA_WIDTH - this.mPlayerTextureRegion
                .getWidth()) / 2;
        final int centerY = (CAMERA_HEIGHT - this.mPlayerTextureRegion
                .getHeight()) / 2;

        final Sprite boisko = new Sprite(CAMERA_WIDTH / 2
                - (this.mBoiskoTextureRegion.getWidth() / 2)
                - this.mBoiskoTextureRegion.getWidth(),
                0 + this.mPlayerTextureRegion.getWidth(),
                this.mBoiskoTextureRegion);
        final Sprite boisko2 = new Sprite(CAMERA_WIDTH / 2
                - (this.mBoiskoTextureRegion.getWidth() / 2),
                0 + this.mPlayerTextureRegion.getWidth(),
                this.mBoisko2TextureRegion);
        final Sprite boisko3 = new Sprite(CAMERA_WIDTH / 2
                + (this.mBoiskoTextureRegion.getWidth() / 2),
                0 + this.mPlayerTextureRegion.getWidth(),
                this.mBoisko3TextureRegion);
        
        final Sprite TeamA = new Sprite(CAMERA_WIDTH / 2 - 35,
                0 + 10, this.mTeamATextureRegion);
        
        final Sprite TeamB = new Sprite(CAMERA_WIDTH / 2 + 60,
                0 + 10, this.mTeamBTextureRegion);

        // final Sprite slupek = new
        // Sprite(CAMERA_WIDTH/2-(this.mBoiskoTextureRegion.getWidth()/2)-this.mBoiskoTextureRegion.getWidth(),(float)
        // (0 + this.mPlayerTextureRegion.getWidth()
        // +0.30*this.mBoiskoTextureRegion.getHeight()),
        // this.mSlupekTextureRegion);
        final Shape slupek = new Rectangle(
                CAMERA_WIDTH / 2 - (this.mBoiskoTextureRegion.getWidth() / 2)
                        - this.mBoiskoTextureRegion.getWidth(),
                (float) (0 + this.mPlayerTextureRegion.getWidth() + 0.30 * this.mBoiskoTextureRegion
                        .getHeight()), 2, 2);
        final Shape slupek2 = new Rectangle(
                CAMERA_WIDTH / 2 + (this.mBoiskoTextureRegion.getWidth() / 2)
                        + this.mBoiskoTextureRegion.getWidth() - 2,
                (float) (0 + this.mPlayerTextureRegion.getWidth() + 0.30 * this.mBoiskoTextureRegion
                        .getHeight()), 2, 2);
        final Shape slupek3 = new Rectangle(
                CAMERA_WIDTH / 2 - (this.mBoiskoTextureRegion.getWidth() / 2)
                        - this.mBoiskoTextureRegion.getWidth(),
                (float) (0 + this.mPlayerTextureRegion.getWidth() + 0.6914 * this.mBoiskoTextureRegion
                        .getHeight()), 2, 2);
        final Shape slupek4 = new Rectangle(
                CAMERA_WIDTH / 2 + (this.mBoiskoTextureRegion.getWidth() / 2)
                        + this.mBoiskoTextureRegion.getWidth() - 2,
                (float) (0 + this.mPlayerTextureRegion.getWidth() + 0.6914 * this.mBoiskoTextureRegion
                        .getHeight()), 2, 2);

        final Shape goalLeft = new Rectangle(
                CAMERA_WIDTH / 2 - (this.mBoiskoTextureRegion.getWidth() / 2)
                        - this.mBoiskoTextureRegion.getWidth() - 16,
                (float) (0 + this.mPlayerTextureRegion.getWidth() + (0.30 * this.mBoiskoTextureRegion
                        .getHeight())), 2,
                (float) (0.40 * this.mBoiskoTextureRegion.getHeight()));
        final Shape goalLeftUp = new Rectangle(
                CAMERA_WIDTH / 2 - (this.mBoiskoTextureRegion.getWidth() / 2)
                        - this.mBoiskoTextureRegion.getWidth() - 16,
                (float) (0 + this.mPlayerTextureRegion.getWidth() + (0.30 * this.mBoiskoTextureRegion
                        .getHeight())), 16, 2);
        final Shape goalLeftDown = new Rectangle(
                CAMERA_WIDTH / 2 - (this.mBoiskoTextureRegion.getWidth() / 2)
                        - this.mBoiskoTextureRegion.getWidth() - 16,
                (float) (0 + this.mPlayerTextureRegion.getWidth() + 0.6914 * this.mBoiskoTextureRegion
                        .getHeight()), 16, 2);

        final Shape goalRight = new Rectangle(
                CAMERA_WIDTH / 2 + (this.mBoiskoTextureRegion.getWidth() / 2)
                        + this.mBoiskoTextureRegion.getWidth() + 16,
                (float) (0 + this.mPlayerTextureRegion.getWidth() + (0.30 * this.mBoiskoTextureRegion
                        .getHeight())), 2,
                (float) (0.40 * this.mBoiskoTextureRegion.getHeight()));
        final Shape goalRightUp = new Rectangle(
                CAMERA_WIDTH / 2 + (this.mBoiskoTextureRegion.getWidth() / 2)
                        + this.mBoiskoTextureRegion.getWidth(),
                (float) (0 + this.mPlayerTextureRegion.getWidth() + (0.30 * this.mBoiskoTextureRegion
                        .getHeight())), 16, 2);
        final Shape goalRightDown = new Rectangle(
                CAMERA_WIDTH / 2 + (this.mBoiskoTextureRegion.getWidth() / 2)
                        + this.mBoiskoTextureRegion.getWidth(),
                (float) (0 + this.mPlayerTextureRegion.getWidth() + 0.6914 * this.mBoiskoTextureRegion
                        .getHeight()), 16, 2);

        
        slupek.setColor(94/255f, 42/255f, 170/255f);
        slupek3.setColor(94/255f, 42/255f, 170/255f);
        goalLeft.setColor(94/255f, 42/255f, 170/255f);
        goalLeftUp.setColor(94/255f, 42/255f, 170/255f);
        goalLeftDown.setColor(94/255f, 42/255f, 170/255f);
        
        slupek2.setColor(1.0f, 0.0f, 0.0f);
        slupek4.setColor(1.0f, 0.0f, 0.0f);
        goalRight.setColor(1.0f, 0.0f, 0.0f);
        goalRightUp.setColor(1.0f, 0.0f, 0.0f);
        goalRightDown.setColor(1.0f, 0.0f, 0.0f);
        
        
        
        final Sprite kickButton = new Sprite(0, CAMERA_HEIGHT
                - this.mOnScreenControlKickTextureRegion.getHeight(),
                this.mOnScreenControlKickTextureRegion) {
            @Override
            public boolean onAreaTouched(final TouchEvent pSceneTouchEvent,
                    final float pTouchAreaLocalX, final float pTouchAreaLocalY) {
                MultiplayerActivity.this.runOnUpdateThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (mServerConnector != null) {
                                mServerConnector
                                        .sendClientMessage(new KickClientMessage());
                                Debug.i("controlled! kick!");
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
                return true;
            }
        };

        scene.registerTouchArea(kickButton);
        scene.setTouchAreaBindingEnabled(true);

        // ---------------------------------------------Time measure method
        // ---------------------------------------------------

        final ChangeableText elapsedText = new ChangeableText(10, 5,
                this.mFont, "00:00", "xxxxx".length());
        final ChangeableText endScore = new ChangeableText(200 , 130,
    			this.tFont , "Your Score is: ", "xxxxxxxxxxxxxxx".length());
        mEngine.registerUpdateHandler(new TimerHandler(1f, true,
                new ITimerCallback() {
                    @Override
                    public void onTimePassed(final TimerHandler pTimerHandler) {
                    	if(seconds == 1 && minutes == 0){
                    		repeat(1/4,0);
                    	}
                    	seconds += 1;
                        if (seconds == 60) {
                            seconds = 0;
                            minutes += 1;
                        }

                        elapsedText.setText(String.format("%02d:%02d", minutes,
                                seconds));
                        if (minutes == 5) {
                        	runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    MultiplayerActivity.this.showDialog(DIALOG_GAMEOVER_ID);
                                }
                            });
                        }
                        
                    }
                }));
        // ----------------------------------------------------------------------------------------------------------------------

        //Zliczanie punktow
        final ChangeableText scoreText = new ChangeableText(
                (CAMERA_WIDTH / 2 - 10), 5, this.mFont, "00-00", "xxxxx".length());
        mEngine.registerUpdateHandler(new TimerHandler(1f, true,
                new ITimerCallback() {
                    @Override
                    public void onTimePassed(final TimerHandler pTimerHandler) {
                        // wykrycie kolizji
                        scoreText.setText(String.format("%02d-%02d",
                                (int) team_a_score, (int) team_b_score));

                    }
                }));

        // -------------------------------------------------------------------------------------------------------------------------

        scene.attachChild(boisko);
        scene.attachChild(boisko2);
        scene.attachChild(boisko3);
        scene.attachChild(TeamA);
        scene.attachChild(TeamB);
        scene.attachChild(slupek);
        scene.attachChild(slupek2);
        scene.attachChild(slupek3);
        scene.attachChild(slupek4);
        scene.attachChild(kickButton);
        scene.attachChild(elapsedText);
        scene.attachChild(scoreText);
        scene.attachChild(goalLeft);
        scene.attachChild(goalLeftUp);
        scene.attachChild(goalLeftDown);
        scene.attachChild(goalRight);
        scene.attachChild(goalRightUp);
        scene.attachChild(goalRightDown);

        final AnimatedSprite face = new AnimatedSprite(0, 0,
                this.mPlayerTextureRegion);
        final AnimatedSprite soccer = new AnimatedSprite(centerX - 30,
                centerY - 30, this.mSoccerTextureRegion);

        
        final int y1base = CAMERA_HEIGHT
                - this.mOnScreenControlBaseTextureRegion.getHeight();
        final int y1 = (this.mPlaceOnScreenControlsAtDifferentVerticalLocations) ? 0
                : y1base;
        final int x1 = CAMERA_WIDTH
                - this.mOnScreenControlBaseTextureRegion.getWidth();

        final AnalogOnScreenControl velocityOnScreenControl = new AnalogOnScreenControl(
                x1, y1, this.mCamera, this.mOnScreenControlBaseTextureRegion,
                this.mOnScreenControlKnobTextureRegion, 0.025f,
                new IAnalogOnScreenControlListener() {
                    public void onControlChange(
                            final BaseOnScreenControl pBaseOnScreenControl,
                            final float pValueX, final float pValueY) {
                        float velX = 0;
                        float velY = 0;
                        float x = 0, y = 0;
                        if (pValueX > 0) {
                            velX = MultiplayerActivity.this.playerVelocity;
                        } else if (pValueX < 0) {
                            velX = -MultiplayerActivity.this.playerVelocity;
                        }
                        if (pValueY > 0) {
                            velY = MultiplayerActivity.this.playerVelocity;
                        } else if (pValueY < 0) {
                            velY = -MultiplayerActivity.this.playerVelocity;
                        }
                        repeat(pValueX, pValueY);
                    }

                    public void onControlClick(
                            final AnalogOnScreenControl pAnalogOnScreenControl) {
                        /* Nothing. */
                    }
                });
        velocityOnScreenControl.getControlBase().setBlendFunction(
                GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
        velocityOnScreenControl.getControlBase().setAlpha(alpha);

        scene.setChildScene(velocityOnScreenControl);

        scene.registerUpdateHandler(new IUpdateHandler() {
            public void reset() {
            }

            @Override
            public void onUpdate(final float arg0) {

            		
                // TODO Auto-generated method, kolizje itp.
            }

        });

        return this.scene;

    }

    /**
     * onLoadComplete.
     */
    @Override
    public void onLoadComplete() {
        /*
         * Telling the device to not go to sleep
         */
        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        wl = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "NX Ball Lock");
        wl.acquire();

        /*
         * Show dialog - choose server or client
         */
        this.showDialog(DIALOG_CHOOSE_SERVER_OR_CLIENT_ID);
    }

    /**
     * onCreateDialog
     * @param pID int
     */
    @Override
    protected Dialog onCreateDialog(final int pID) {
        switch (pID) {
        case DIALOG_SHOW_SERVER_IP_ID:
            try {
                return new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle("Your Server-IP ...")
                        .setCancelable(false)
                        .setMessage(
                                "The IP of your Server is:\n"
                                        + WifiUtils.getWifiIPv4Address(this))
                        .setPositiveButton(android.R.string.ok, null).create();
            } catch (final UnknownHostException e) {
                return new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle("Your Server-IP ...")
                        .setCancelable(false)
                        .setMessage("Error retrieving IP of your Server: " + e)
                        .setPositiveButton(android.R.string.ok,
                                new OnClickListener() {
                                    @Override
                                    public void onClick(
                                            final DialogInterface pDialog,
                                            final int pWhich) {
                                        MultiplayerActivity.this.finish();
                                    }
                                }).create();
            }
        case DIALOG_ENTER_SERVER_IP_ID:
            final EditText ipEditText = new EditText(this);
            try {
				ipEditText.setText(IpAdressShower(WifiUtils.getWifiIPv4Address(this).toString())) ;
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            return new AlertDialog.Builder(this)
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setTitle("Enter Server-IP ...")
                    .setCancelable(false)
                    .setView(ipEditText)
                    .setPositiveButton("Connect", new OnClickListener() {
                        @Override
                        public void onClick(final DialogInterface pDialog,
                                final int pWhich) {
                            MultiplayerActivity.this.serverIP = ipEditText
                                    .getText().toString();
                            MultiplayerActivity.this.initClient();
                        }
                    })
                    .setNegativeButton(android.R.string.cancel,
                            new OnClickListener() {
                                @Override
                                public void onClick(
                                        final DialogInterface pDialog,
                                        final int pWhich) {
                                    MultiplayerActivity.this.finish();
                                }
                            }).create();
        case DIALOG_CHOOSE_SERVER_OR_CLIENT_ID:
            return new AlertDialog.Builder(this)
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setTitle("Be Server or Host ...").setCancelable(false)
                    .setPositiveButton("Client", new OnClickListener() {
                        @Override
                        public void onClick(final DialogInterface pDialog,
                                final int pWhich) {
                            MultiplayerActivity.this
                                    .showDialog(DIALOG_ENTER_SERVER_IP_ID);
                        }
                    }).setNegativeButton("Host", new OnClickListener() {
                        @Override
                        public void onClick(final DialogInterface pDialog,
                                final int pWhich) {
                            MultiplayerActivity.this.toast("You are server!");
                            MultiplayerActivity.this.initServer();

                            MultiplayerActivity.this
                                    .showDialog(DIALOG_SHOW_SERVER_IP_ID);
                        }
                    }).create();
        case DIALOG_GAMEOVER_ID:
            
                return new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle("Your Game just End")
                        .setCancelable(false)
                        .setMessage(
                                "Your score is:\n"+(int) team_a_score+" : "+(int) team_b_score)
                        .setPositiveButton(android.R.string.ok,  new OnClickListener() {
                            @Override
                            public void onClick(final DialogInterface pDialog,
                                    final int pWhich) {
                            	MultiplayerActivity.this.finish();
                                Intent mainIntent = new Intent().setClass(
                                        MultiplayerActivity.this,
                                        AfterMatchActivity.class);
                                mainIntent.putExtra("team_a_score", (int)team_a_score);
                                mainIntent.putExtra("team_b_score", (int)team_b_score);
                                mainIntent.putExtra("game_time", seconds + (minutes * 60));
                                mainIntent.putExtra("imeis", playersImeis);
                                Debug.i("put imeis");
                                startActivity(mainIntent);
                            }
                        }).create();
            
        default:
            return super.onCreateDialog(pID);
        }
    }

    /**
     * repeat.
     * @param x
     * @param y
     */
    public void repeat(float x, float y) {
        // short control = Short.MIN_VALUE;
        // short control2 = Short.MIN_VALUE;
       // Debug.i("controlled! client x wynosi " + x + " y " + y);
        /*
         * int xx=0,yy=0; if (x<-0.3 && y<0.3 && y>-0.3) { xx = -3; yy = 0; }
         * else if (x>0.3 && y<0.3 && y>-0.3) { xx = 3; yy = 0; } else if
         * (y<-0.3 && x<0.3 && x>-0.3) { xx = 0; yy = -3; } else if (y>0.3 &&
         * x<0.3 && x>-0.3) { xx = 0; yy = 3; } else if (x>0.3 && y<=-0.3 &&
         * y>=-1) { xx = 3; yy = -3; } else if (x>=0.3 && y>=0.3 && y<=1) { xx =
         * 3; yy = 3; } else if (x<-0.3 && y>=0.3 && y<=1) { xx = -3; yy = 3; }
         * else if (x<-0.3 && y<=-0.3 && y>=-1) { xx = -3; yy = -3; }
         */

        if (x != 0 || y != 0) {
            try {
                if (mServerConnector != null) {
                    mServerConnector
                            .sendClientMessage(new ControlClientMessage(x * 4,
                                    y * 4));

                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    /**
     * initServer - Server init.
     */
    public void initServer() {
        
        server = new Server(SERVER_PORT, this);
        server.start();
        this.mEngine.registerUpdateHandler(server);

        /*
         * To be sure that server is started
         */
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        initClient();
    }

    /**
     * initClient - Client init.
     */
    public void initClient() {

        players = new ArrayList<AnimatedSprite>();
        balls = new ArrayList<AnimatedSprite>();
        try {
            this.mServerConnector = new SocketConnectionServerConnector(
                    new SocketConnection(new Socket(this.serverIP, SERVER_PORT)),
                    new ExampleServerConnectorListener());

            this.mServerConnector.registerServerMessage(
                    FLAG_MESSAGE_SERVER_CONNECTION_CLOSE,
                    ConnectionCloseServerMessage.class,
                    new IServerMessageHandler<SocketConnection>() {
                        @Override
                        public void onHandleMessage(
                                final ServerConnector<SocketConnection> pServerConnector,
                                final IServerMessage pServerMessage)
                                throws IOException {
                            MultiplayerActivity.this.finish();
                        }
                    });

            this.mServerConnector.registerServerMessage(
                    FLAG_MESSAGE_SERVER_GAMESTATE_CHANGE,
                    GamestateChangeServerMessage.class,
                    new IServerMessageHandler<SocketConnection>() {
                        @Override
                        public void onHandleMessage(
                                final ServerConnector<SocketConnection> pServerConnector,
                                final IServerMessage pServerMessage)
                                throws IOException {
                            final GamestateChangeServerMessage gamestateChangeServerMessage = (GamestateChangeServerMessage) pServerMessage;
                            MultiplayerActivity.this
                                    .gameChanged(gamestateChangeServerMessage
                                            .getPositions());
                        }
                    });

            this.mServerConnector.registerServerMessage(
                    FLAG_MESSAGE_SERVER_BALL_CHANGE,
                    BallChangeServerMessage.class,
                    new IServerMessageHandler<SocketConnection>() {
                        @Override
                        public void onHandleMessage(
                                final ServerConnector<SocketConnection> pServerConnector,
                                final IServerMessage pServerMessage)
                                throws IOException {
                            final BallChangeServerMessage ballChangeServerMessage = (BallChangeServerMessage) pServerMessage;
                            MultiplayerActivity.this
                                    .ballChanged(ballChangeServerMessage
                                            .getSoccer());
                        }
                    });

            this.mServerConnector.registerServerMessage(
                    FLAG_MESSAGE_SERVER_SCORE_CHANGE,
                    ScoreChangeServerMessage.class,
                    new IServerMessageHandler<SocketConnection>() {
                        @Override
                        public void onHandleMessage(
                                final ServerConnector<SocketConnection> pServerConnector,
                                final IServerMessage pServerMessage)
                                throws IOException {
                            final ScoreChangeServerMessage scoreChangeServerMessage = (ScoreChangeServerMessage) pServerMessage;
                            MultiplayerActivity.this.scoreChanged(
                                    scoreChangeServerMessage.getWynikLewo(),
                                    scoreChangeServerMessage.getWynikPrawo());
                        }
                    });
            
            this.mServerConnector.registerServerMessage(
            		FLAG_MESSAGE_SERVER_PLAYERS_DATA,
                    PlayersDataServerMessage.class,
                    new IServerMessageHandler<SocketConnection>() {
                        @Override
                        public void onHandleMessage(
                                final ServerConnector<SocketConnection> pServerConnector,
                                final IServerMessage pServerMessage)
                                throws IOException {
                            final PlayersDataServerMessage playersDataMessage = (PlayersDataServerMessage) pServerMessage;
                            playersImeis =  playersDataMessage.getImeis();
                            Debug.i("all clients imeis received");
                        }
                    });

            this.mServerConnector.getConnection().start();
        } catch (final Throwable t) {
            Debug.e(t);
        }
    }

    /**
     * This method is called when we recieved GamestateChange Message from
     * server
     * *
     * @param positions
     */
    protected void gameChanged(Collection<Vector2> positions) {
        Debug.i("gameChanged STARTED");

        if (positions.size() < players.size()) {
            AnimatedSprite player = players.remove(0);
            scene.detachChild(player);

        }

        else
            while (positions.size() > players.size()) {
            	if (players.isEmpty())
            	{
            		AnimatedSprite player = new AnimatedSprite(0, 0,
                            mPlayerTextureRegion);
                	players.add(player);
                    scene.attachChild(player);
            	}
            	else
            	{
            		AnimatedSprite player = new AnimatedSprite(0, 0,
                            mPlayerTextureRegion2);
                	players.add(player);
                    scene.attachChild(player);
            	}
            }
        Iterator<Vector2> itPositions = positions.iterator();
        Iterator<AnimatedSprite> itPlayers = players.iterator();
        while (itPositions.hasNext() && itPlayers.hasNext()) {

            AnimatedSprite player = itPlayers.next();
            Vector2 position = itPositions.next();
            Debug.d("Pozycje " + position.x + " " + position.y);
            player.setPosition(position.x, position.y);

        }

        Debug.d("gameChanged ENDED");

    }

    /**
     * This method is called when we recieved BallChange Message from server
     * 
     * @param
     */
    protected void ballChanged(Vector2 soccer) {
        if (czyJestPilka == 0) {
            AnimatedSprite sprite2 = new AnimatedSprite(100, 100,
                    mSoccerTextureRegion);
            scene.attachChild(sprite2);
            balls.add(sprite2);
            czyJestPilka = 1;
        }
        balls.get(0).setPosition(soccer.x, soccer.y);
    }

    /**
     * This method is called when we recieved ScoreChange Message from server
     * 
     * @param
     */
    protected void scoreChanged(float wynikLewo2, float wynikPrawo2) {

        team_a_score = wynikPrawo2;
        // team_b_score = wynikPrawo2;
        team_b_score = wynikLewo2;
    }

    void toast(final String pMessage) {
        Debug.d(pMessage);
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(MultiplayerActivity.this, pMessage,
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * onDestroy.
     */
    @Override
    protected void onDestroy() {
        if (server != null) {
            server.terminate();
        }

        super.onDestroy();
    }

    /**
     * Listening to connection states Class
     */
    private class ExampleServerConnectorListener implements
            ISocketConnectionServerConnectorListener {
        /**
         * onStarted.
         * @param pConnector
         */
        @Override
        public void onStarted(final ServerConnector<SocketConnection> pConnector) {
            MultiplayerActivity.this.toast("CLIENT: Connected to server.");
            Debug.i("CLIENT: Connected to server.");
            TelephonyManager telephonyManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
            String imei = telephonyManager.getDeviceId();
            try {
                if (mServerConnector != null) {
                    mServerConnector
                            .sendClientMessage(new PlayerDataClientMessage(imei));
                    Debug.i("Player IMEI sent");
                    Debug.i("imei sent " + imei);
                    mServerConnector
                    	    .sendClientMessage(new ControlClientMessage(1,
                            0));
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        /**
         * onTerminated
         * @param pConnector.
         */
        @Override
        public void onTerminated(
                final ServerConnector<SocketConnection> pConnector) {
            MultiplayerActivity.this
                    .toast("CLIENT: Disconnected from Server...");
            MultiplayerActivity.this.finish();
            wl.release();
        }
    }

}
