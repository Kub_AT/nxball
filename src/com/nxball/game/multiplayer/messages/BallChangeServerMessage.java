package com.nxball.game.multiplayer.messages;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import org.anddev.andengine.extension.multiplayer.protocol.adt.message.server.ServerMessage;
import org.anddev.andengine.util.Debug;

import com.badlogic.gdx.math.Vector2;

/**
 * BallChangeServerMessage class.
 *
 */
public class BallChangeServerMessage extends ServerMessage implements
        MessageFlags {

    private Vector2 soccer;
    private float wLewo;
    private float wPrawo;

    /**
     * BallChangeServerMessage.
     */
    public BallChangeServerMessage() {
        Vector2 soccer = new Vector2();

        // soccer new Vector2;
    }

    /**
     * BallChangeServerMessage.
     * @param ball
     */
    public BallChangeServerMessage(Vector2 ball) {
        this.soccer = ball;
    }

    /**
     * getSoccer.
     * @return
     */
    public Vector2 getSoccer() {
        return soccer;
    }

    /**
     * getFlag.
     */
    @Override
    public short getFlag() {
        return FLAG_MESSAGE_SERVER_BALL_CHANGE;
    }

    /**
     * onReadTransmissionData.
     * @param pDataInputStream
     * @throws IOExcetion
     */
    @Override
    protected void onReadTransmissionData(final DataInputStream pDataInputStream)
            throws IOException {
        float x = pDataInputStream.readFloat();
        float y = pDataInputStream.readFloat();
        soccer = new Vector2(x, y);
        float z = pDataInputStream.readFloat();
        wLewo = z;
        float zz = pDataInputStream.readFloat();
        wPrawo = zz;
        // soccer.x=x;
        // soccer.y=y;
    }

    /**
     * onWriteTransmissionData
     * @param pDataInputStream
     * @throws IOExcetion
     */
    @Override
    protected void onWriteTransmissionData(
            final DataOutputStream pDataOutputStream) throws IOException {
        pDataOutputStream.writeFloat(soccer.x);
        pDataOutputStream.writeFloat(soccer.y);
        pDataOutputStream.writeFloat(wLewo);
        pDataOutputStream.writeFloat(wPrawo);

    }
}