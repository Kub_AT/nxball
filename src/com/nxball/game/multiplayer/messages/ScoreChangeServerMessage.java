package com.nxball.game.multiplayer.messages;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import org.anddev.andengine.extension.multiplayer.protocol.adt.message.server.ServerMessage;
import org.anddev.andengine.util.Debug;

import com.badlogic.gdx.math.Vector2;

/**
 * ScoreChangeServerMessage class.
 *
 */
public class ScoreChangeServerMessage extends ServerMessage implements MessageFlags {

		private float wLewo;
		private float wPrawo;
		
		/**
		 * ScoreChangeServerMessage.
		 */
		public ScoreChangeServerMessage() {
			wLewo = 0;
			wPrawo = 0;
			//soccer new Vector2;
		}

		/**
		 * ScoreChangeServerMessage.
		 * @param wynikLewo
		 * @param wynikPrawo
		 */
		public ScoreChangeServerMessage(float wynikLewo, float wynikPrawo) {
			this.wLewo = wynikLewo;
			this.wPrawo = wynikPrawo;
		}

		/**
		 * getWynikLewo.
		 * @return wLewo
		 */
		public float getWynikLewo() {
			return wLewo;
		}
		
		/**
		 * getWynikPrawo.
		 * @return wPrawo
		 */
		public float getWynikPrawo() {
			return wPrawo;
		}

		/**
		 * getFlag.
		 */
		@Override
		public short getFlag() {
			return FLAG_MESSAGE_SERVER_SCORE_CHANGE;
		}
		
	    /**
	     * onReadTransmissionData.
	     * @param pDataOutputStream
	     * @throws IOException
	     */
		@Override
		protected void onReadTransmissionData(
				final DataInputStream pDataInputStream) throws IOException {
			float z = pDataInputStream.readFloat();
			wLewo = z;
			float zz = pDataInputStream.readFloat();
			wPrawo = zz;
			//soccer.x=x;
			//soccer.y=y;
		}

	    /**
	     * onWriteTransmissionData
	     * @param pDataOutputStream
	     * @throws IOException
	     */
		@Override
		protected void onWriteTransmissionData(
				final DataOutputStream pDataOutputStream) throws IOException {
			pDataOutputStream.writeFloat(wLewo);
			pDataOutputStream.writeFloat(wPrawo);	
			
		}
	}