package com.nxball.game.multiplayer.messages;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import org.anddev.andengine.extension.multiplayer.protocol.adt.message.server.ServerMessage;

import com.badlogic.gdx.math.Vector2;

/**
 * BallChangeServerMessage class.
 *
 */
public class PlayersDataServerMessage extends ServerMessage implements
        MessageFlags {

	ArrayList<String> playerImei;
	int length1 = 15;
	int length2 = 15;

    /**
     * BallChangeServerMessage.
     */
    public PlayersDataServerMessage() {
        Vector2 soccer = new Vector2();

        // soccer new Vector2;
    }

    /**
     * BallChangeServerMessage.
     * @param ball
     */
    public PlayersDataServerMessage(ArrayList<String> imeis) {
        this.playerImei = imeis;
    }

    /**
     * getSoccer.
     * @return
     */
    public ArrayList<String> getImeis() {
        return playerImei;
    }

    /**
     * getFlag.
     */
    @Override
    public short getFlag() {
        return FLAG_MESSAGE_SERVER_PLAYERS_DATA;
    }

    /**
     * onReadTransmissionData.
     * @param pDataInputStream
     * @throws IOExcetion
     */
    @Override
    protected void onReadTransmissionData(final DataInputStream pDataInputStream)
            throws IOException {
    	String imei1 = "";
    	String imei2 = "";

    	for (int i=0;i<15;i++) {
    		imei1 += pDataInputStream.readChar();
    	}
    	for (int i=0;i<15;i++) {
    		imei2 += pDataInputStream.readChar();
    	}
    	playerImei = new ArrayList<String>();
    	playerImei.add(imei1);
    	playerImei.add(imei2);

    }

    /**
     * onWriteTransmissionData
     * @param pDataInputStream
     * @throws IOExcetion
     */
    @Override
    protected void onWriteTransmissionData(
            final DataOutputStream pDataOutputStream) throws IOException {
    	if (playerImei.size() >= 2) {
    		pDataOutputStream.writeChars(playerImei.get(0));
    		pDataOutputStream.writeChars(playerImei.get(1));
    	} else if (playerImei.size() == 1) {
    		pDataOutputStream.writeChars(playerImei.get(0));
    		pDataOutputStream.writeChars("098765432100000");
    		
    	} else {
    		pDataOutputStream.writeChars("123456789000000");
    		pDataOutputStream.writeChars("098765432100000");
    	}
    }
}