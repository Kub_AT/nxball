package com.nxball.game.multiplayer.messages;

/**
 * MessageFlags interface.
 *
 */
public interface MessageFlags {
    public static final short FLAG_MESSAGE_SERVER_CONNECTION_CLOSE = 1;
    public static final short FLAG_MESSAGE_SERVER_GAMESTATE_CHANGE = FLAG_MESSAGE_SERVER_CONNECTION_CLOSE + 1;
    public static final short FLAG_MESSAGE_CLIENT_CONNECTION_CLOSE = FLAG_MESSAGE_SERVER_GAMESTATE_CHANGE + 1;
    public static final short FLAG_MESSAGE_CLIENT_CONTROL = FLAG_MESSAGE_CLIENT_CONNECTION_CLOSE + 1;
    public static final short FLAG_MESSAGE_CLIENT_KICK = FLAG_MESSAGE_CLIENT_CONTROL + 1;
    public static final short FLAG_MESSAGE_SERVER_BALL_CHANGE = FLAG_MESSAGE_CLIENT_KICK + 1;
    public static final short FLAG_MESSAGE_SERVER_SCORE_CHANGE = FLAG_MESSAGE_SERVER_BALL_CHANGE + 1;
    public static final short FLAG_MESSAGE_CLIENT_PLAYER_DATA = FLAG_MESSAGE_SERVER_SCORE_CHANGE + 1;
    public static final short FLAG_MESSAGE_SERVER_PLAYERS_DATA = FLAG_MESSAGE_CLIENT_PLAYER_DATA + 1;
}
