package com.nxball.game.multiplayer.messages;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import org.anddev.andengine.extension.multiplayer.protocol.adt.message.client.ClientMessage;

/**
 * 
 * KickClientMessage class
 * 
 */
public class PlayerDataClientMessage extends ClientMessage implements MessageFlags {
	
	String imei;
    
    /**
     * PlayerDatalientMessage.
     */
    public PlayerDataClientMessage() {
        //
    }
    
    /**
     * PlayerDatalientMessage.
     */
    public PlayerDataClientMessage(String imei) {
        this.imei = imei;
    }

    /**
     * getFlag.
     */
    @Override
    public short getFlag() {
        return FLAG_MESSAGE_CLIENT_PLAYER_DATA;
    }
    
    public String getIMEI() {
    	return imei;
    }

    /**
     * onReadTransmissionData.
     * @param pDataOutputStream
     * @throws IOException
     */
    @Override
    protected void onReadTransmissionData(final DataInputStream pDataInputStream)
            throws IOException {
    	imei = "";
    	for (int i=0;i<15;i++) {
    		imei += pDataInputStream.readChar();
    	}
    }

    /**
     * onWriteTransmissionData.
     * @param pDataOutputStream
     * @throws IOException
     */
    @Override
    protected void onWriteTransmissionData(
            final DataOutputStream pDataOutputStream) throws IOException {
    	pDataOutputStream.writeChars(imei);
    }
}
