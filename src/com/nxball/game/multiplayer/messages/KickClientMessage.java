package com.nxball.game.multiplayer.messages;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import org.anddev.andengine.extension.multiplayer.protocol.adt.message.client.ClientMessage;

/**
 * 
 * KickClientMessage class
 * 
 */
public class KickClientMessage extends ClientMessage implements MessageFlags {
    
    /**
     * KickClientMessage.
     */
    public KickClientMessage() {
        //
    }

    /**
     * getFlag.
     */
    @Override
    public short getFlag() {
        return FLAG_MESSAGE_CLIENT_KICK;
    }

    /**
     * onReadTransmissionData.
     * @param pDataOutputStream
     * @throws IOException
     */
    @Override
    protected void onReadTransmissionData(final DataInputStream pDataInputStream)
            throws IOException {
        /* Nothing to read. */
    }

    /**
     * onWriteTransmissionData.
     * @param pDataOutputStream
     * @throws IOException
     */
    @Override
    protected void onWriteTransmissionData(
            final DataOutputStream pDataOutputStream) throws IOException {
        /* Nothing to write. */
    }
}
