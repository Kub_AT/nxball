
package com.nxball.game.multiplayer.messages;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import org.anddev.andengine.extension.multiplayer.protocol.adt.message.server.ServerMessage;

/**
 * ConnectionCloseClientMessage class.
 *
 */
public class ConnectionCloseClientMessage extends ServerMessage implements MessageFlags{
	public ConnectionCloseClientMessage() {

	}

	/**
	 * getFlag.
	 */
	@Override
	public short getFlag() {
		return FLAG_MESSAGE_CLIENT_CONNECTION_CLOSE;
	}

	/**
	 * onReadTransmissionData.
     * @param pDataOutputStream
     * @throws IOException
	 */
	@Override
	protected void onReadTransmissionData(
			final DataInputStream pDataInputStream) throws IOException {
		/* Nothing to read. */
	}

	/**
	 * onWriteTransmissionData
	 * @param pDataOutputStream
	 * @throws IOException
	 */
	@Override
	protected void onWriteTransmissionData(
			final DataOutputStream pDataOutputStream) throws IOException {
		/* Nothing to write. */
	}
}
