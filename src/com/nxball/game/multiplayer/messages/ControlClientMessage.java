package com.nxball.game.multiplayer.messages;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import org.anddev.andengine.extension.multiplayer.protocol.adt.message.client.ClientMessage;

/**
 * ControlClientMessage class.
 * 
 */
public class ControlClientMessage extends ClientMessage implements MessageFlags {
    public static final short UP = 0;
    public static final short RIGHT = 1;
    public static final short DOWN = 2;
    public static final short LEFT = 3;
    public static final short UPRIGHT = 4;
    public static final short DOWNRIGHT = 5;
    public static final short DOWNLEFT = 6;
    public static final short UPLEFT = 7;

    private float x;
    private float y;

    private short move;

    /**
     * ControlClientMessage.
     */
    public ControlClientMessage() {
        //
    }

    /**
     * ControlClientMessage. 
     * @param move
     */
    public ControlClientMessage(short move) {
        this.move = move;
    }

    /**
     * set.
     * @param move
     */
    public void set(short move) {
        this.move = move;
    }

    /**
     * getMove.
     * @return move
     */
    public short getMove() {
        return move;
    }

    /**
     * ControlClientMessage.
     * @param x
     * @param y
     */
    public ControlClientMessage(float x, float y) {
        this.x = x;
        this.y = y;
    }

    /**
     * set2
     * @param x
     * @param y
     */
    public void set2(float x, float y) {
        this.x = x;
        this.y = y;
    }

    /**
     * getMove2
     * @return x
     */
    public float getMove2() {
        return x;
    }

    /**
     * getMove3
     * @return y
     */ 
    public float getMove3() {
        return y;
    }

    /**
     * getFlag
     */
    @Override
    public short getFlag() {
        return FLAG_MESSAGE_CLIENT_CONTROL;
    }

    /**
     * onReadTransmissionData.
     * @param pDataOutputStream
     * @throws IOException
     */
    @Override
    protected void onReadTransmissionData(final DataInputStream pDataInputStream)
            throws IOException {
        x = pDataInputStream.readFloat();
        y = pDataInputStream.readFloat();
    }

    /**
     * onWriteTransmissionData.
     * @param pDataOutputStream
     * @throws IOException
     */
    @Override
    protected void onWriteTransmissionData(
            final DataOutputStream pDataOutputStream) throws IOException {
        pDataOutputStream.writeFloat(x);
        pDataOutputStream.writeFloat(y);
    }
}
