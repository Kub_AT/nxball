package com.nxball.game.multiplayer.messages;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import org.anddev.andengine.extension.multiplayer.protocol.adt.message.server.ServerMessage;
import org.anddev.andengine.util.Debug;

import com.badlogic.gdx.math.Vector2;

/**
 * GamestateChangeServerMessage Class.
 */
public class GamestateChangeServerMessage extends ServerMessage implements
        MessageFlags {

    private Collection<Vector2> positions;

    // private Vector2 soccer;
    // private float wLewo;
    // private float wPrawo;
    
    /**
     * GamestateChangeServerMessage.
     */
    public GamestateChangeServerMessage() {
        positions = new ArrayList<Vector2>();
        // Vector2 soccer = new Vector2();
        // wLewo = 0;
        // wPrawo = 0;
        // soccer new Vector2;
    }

    /**
     * GamestateChangeServerMessage.
     * @param collection
     */
    public GamestateChangeServerMessage(Collection<Vector2> collection) {
        this.positions = collection;
        // this.soccer = ball;
        // this.wLewo = wynikLewo;
        // this.wPrawo = wynikPrawo;
    }

    /**
     * set.
     * @param positions
     */
    public void set(Collection<Vector2> positions) {
        this.positions = positions;
    }

    /**
     * getPositions.
     * @return positions
     */
    public Collection<Vector2> getPositions() {
        return positions;
    }

    /*
     * public Vector2 getSoccer() { return soccer; }
     * 
     * public float getWynikLewo() { return wLewo; }
     * 
     * public float getWynikPrawo() { return wPrawo; }
     */

    /**
     * getFlag.
     */
    @Override
    public short getFlag() {
        return FLAG_MESSAGE_SERVER_GAMESTATE_CHANGE;
    }

    /**
     * onReadTransmissionData.
     * @param pDataOutputStream
     * @throws IOException
     */
    @Override
    protected void onReadTransmissionData(final DataInputStream pDataInputStream)
            throws IOException {
        int n = pDataInputStream.readInt();
        Debug.d("READ players: " + n);
        positions.clear();
        for (int i = 0; i < n; ++i) {

            float x = pDataInputStream.readFloat();
            float y = pDataInputStream.readFloat();
            Debug.i("onnreadtrans gamestate x " + x + " y " + y);
            positions.add(new Vector2(x, y));
        }
        // float x = pDataInputStream.readFloat();
        // float y = pDataInputStream.readFloat();
        // soccer = new Vector2(x,y);
        // float z = pDataInputStream.readFloat();
        // wLewo = z;
        // float zz = pDataInputStream.readFloat();
        // wPrawo = zz;
        // soccer.x=x;
        // soccer.y=y;
    }

    /**
     * onWriteTransmissionData.
     * @param pDataOutputStream
     * @throws IOException
     */
    @Override
    protected void onWriteTransmissionData(
            final DataOutputStream pDataOutputStream) throws IOException {
        pDataOutputStream.writeInt(positions.size());
        Debug.d("SEND players: " + positions.size());
        for (Vector2 pos : positions) {
            Debug.i("onnwritetrans gamestate x " + pos.x + " y " + pos.y);
            pDataOutputStream.writeFloat(pos.x);
            pDataOutputStream.writeFloat(pos.y);
        }
        // pDataOutputStream.writeFloat(soccer.x);
        // pDataOutputStream.writeFloat(soccer.y);
        // pDataOutputStream.writeFloat(wLewo);
        // pDataOutputStream.writeFloat(wPrawo);

    }
}
