package com.nxball.game;

public class User {
    private long id;
    String user;
    String colour;

    /**
     * getId Getter.
     * 
     * @return
     */
    public long getId() {
        return id;
    }

    /**
     * setId Setter.
     * 
     * @param id
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * getUser Getter.
     * 
     * @return
     */
    public String getUser() {
        return user;
    }

    /**
     * getUser Getter.
     * 
     * @param name
     * @return
     */
    public User getUser(String name) {
        if (name == user) {
            User user1 = new User();
            user1.user = user;
            user1.id = user1.getId();
            return user1;
        }
        return null;
    }

    /**
     * setUser Setter.
     * 
     * @param user
     */
    public void setUser(String user) {
        this.user = user;
    }

    /**
     * toString. Will be used by the ArrayAdapter in the ListView.
     * 
     * @return user
     */
    @Override
    public String toString() {
        return user;
    }
}
