package com.nxball.game;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

/**
 * UsersDataSource
 *
 */
public class UsersDataSource {

    // Database fields
    private SQLiteDatabase database;
    private MySQLiteHelper dbHelper;
    private String[] allColumns = { MySQLiteHelper.COLUMN_ID,
            MySQLiteHelper.COLUMN_USER };

    /**
     * UsersDataSource Method.
     * @param context
     */
    public UsersDataSource(Context context) {
        dbHelper = new MySQLiteHelper(context);
    }

    /**
     * open Method.
     * @throws SQLException
     */
    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    /**
     * Close method.
     */
    public void close() {
        dbHelper.close();
    }

    /**
     * createUser Method.
     * @param user
     * @return
     */
    public User createUser(String user) {
        ContentValues values = new ContentValues();
        values.put(MySQLiteHelper.COLUMN_USER, user);
        long insertId = database.insert(MySQLiteHelper.TABLE_USERS, null,
                values);
        Cursor cursor = database.query(MySQLiteHelper.TABLE_USERS, allColumns,
                MySQLiteHelper.COLUMN_ID + " = " + insertId, null, null, null,
                null);
        cursor.moveToFirst();
        User newUser = cursorToUser(cursor);
        cursor.close();
        return newUser;
    }

    /**
     * deleteUser Method.
     * @param user
     */
    public void deleteUser(User user) {
        long id = user.getId();
        database.delete(MySQLiteHelper.TABLE_USERS, MySQLiteHelper.COLUMN_ID
                + " = " + id, null);
    }

    /**
     * getAllUsers.
     * @return users List;
     */
    public List<User> getAllUsers() {
        List<User> users = new ArrayList<User>();

        Cursor cursor = database.query(MySQLiteHelper.TABLE_USERS, allColumns,
                null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            User user = cursorToUser(cursor);
            users.add(user);
            cursor.moveToNext();
        }
        // Make sure to close the cursor
        cursor.close();
        return users;
    }

    /**
     * cursorToUser.
     * @param cursor
     * @return user User
     */
    private User cursorToUser(Cursor cursor) {
        User user = new User();
        user.setId(cursor.getLong(0));
        user.setUser(cursor.getString(1));
        return user;
    }
}
