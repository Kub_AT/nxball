package com.nxball.game;

import android.os.Bundle;
import android.preference.PreferenceActivity;

/**
 * SettingsActivity Class.
 *
 */
public class SettingsActivity extends PreferenceActivity {
	
	/**
	 * Called on Create.
	 * @param savedInstanceState Bundle
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {     
	    super.onCreate(savedInstanceState);        
	    addPreferencesFromResource(R.xml.preferences);        
	}

}
