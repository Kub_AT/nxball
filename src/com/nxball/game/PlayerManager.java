package com.nxball.game;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Simply manager for players.
 * @author Michal
 *
 */
public class PlayerManager {
	
	/**
	 * Players array.
	 */
	private List<Player> players;
	
	/**
	 * Constructor.
	 */
	public PlayerManager() {
		players = new ArrayList<Player>();
	}
	
	/**
	 * Creates and returns new Player object.
	 * 
	 * @param name
	 * @return Player
	 */
	public Player createPlayer(String name, Boolean local)
	{
		if (name != "") {
			if (local == true && getLocal() != null) {
				throw new RuntimeException("You've tried to add second local player.");
			}
			Player player = new Player(name, local);
			players.add(player);
			return player;
		} else {
			throw new IllegalArgumentException("Player name cannot be empty");
		}
	}
	
	/**
	 * getByName Method.
	 * @param name
	 * @return
	 */
	public final Player getByName(String name)
	{
		Iterator<Player> itr = players.iterator(); 
		while(itr.hasNext()) {

		    Player player = itr.next(); 
		    if (player.getName() == name) {
		    	return player;
		    }
		} 
		return null;
	}
	
	/**
	 * Returns all players.
	 * 
	 * @return ArrayList
	 */
	public List<Player> getAll()
	{
		return players;
	}
	
	/**
	 * Returns local player
	 * @return
	 */
	public final Player getLocal()
	{
		Iterator<Player> itr = players.iterator(); 
		while(itr.hasNext()) {

		    Player player = itr.next(); 
		    if (player.isLocal() == true) {
		    	return player;
		    }
		} 
		return null;
	}
	
}
