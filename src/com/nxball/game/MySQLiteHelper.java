package com.nxball.game;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * MySQLiteHelper class.
 *
 */
public class MySQLiteHelper extends SQLiteOpenHelper {

	public static final String TABLE_USERS = "users";
	public static final String COLUMN_ID = "_id";
	public static final String COLUMN_USER = "user";
	public static final String COLUMN_COLOUR = "colour";

	private static final String DATABASE_NAME = "users.db";
	private static final int DATABASE_VERSION = 1;

	/**
	 * Database creation sql statement
	 */
	private static final String DATABASE_CREATE = "create table "
			+ TABLE_USERS + "( " + COLUMN_ID
			+ " integer primary key autoincrement, " + COLUMN_USER
			+ " text not null);";
	
	/**
	 * MySQLiteHelper method.
	 * @param context
	 */
	public MySQLiteHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	/**
	 * Called when the activity is first created.
	 * @param savedInstanceState Bundle
	 */
	@Override
	public void onCreate(SQLiteDatabase database) {
		database.execSQL(DATABASE_CREATE);
	}

	/**
	 * Called when upgrade
	 * @param SQLiteDatabase db
	 * @param oldVersion int
	 * @param newVersion int
	 */
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.w(MySQLiteHelper.class.getName(),
				"Upgrading database from version " + oldVersion + " to "
						+ newVersion + ", which will destroy all old data");
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_USERS);
		onCreate(db);
	}

}