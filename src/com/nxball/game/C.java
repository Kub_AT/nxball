package com.nxball.game;

/**
 * This interface inlcudes common constants, used in many classes
 * 
 * @author Daniel Mroczek
 * 
 */
public interface C {
    public static final int t4x = 4;
    public static final int t16x = 16;
    public static final int t32x = 32;
    public static final int t64x = 64;
    public static final int t128x = 128;
    public static final int t256x = 256;
    public static final int t512x = 512;

    public static final int CAMERA_WIDTH = 480;
    public static final int CAMERA_HEIGHT = 320;
}
