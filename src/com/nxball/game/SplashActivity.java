package com.nxball.game;

import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

/**
 * Splash Screen Class.
 * 
 * @author KubAT
 * 
 */
public class SplashActivity extends Activity {
    /**
     * Splash Delay 3000 = 3sec.
     */
    private final long splashDelay = 5000;

    /**
     * Called on Create.
     * 
     * @param savedInstanceState
     *            Bundle
     * @see android.app.Activity#onCreate(android.os.Bundle)
     */
    public final void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);

        TimerTask task = new TimerTask() {
            public void run() {
                finish();
                Intent mainIntent = new Intent().setClass(SplashActivity.this,
                        MenuScrollerActivity.class);
                startActivity(mainIntent);
            }
        };
        Timer timer = new Timer();
        timer.schedule(task, splashDelay);
    }
}