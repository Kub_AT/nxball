package com.nxball.game;

import java.util.List;
import java.util.Random;

import android.app.ListActivity;
import android.content.Intent;
import android.inputmethodservice.ExtractEditText;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import java.util.*;

//import android.widget.EditText;

/**
 * TestDatabaseActivity class.
 * 
 */
public class TestDatabaseActivity extends ListActivity {
    private UsersDataSource datasource;

    /**
     * Called when the activity is first created.
     * 
     * @param savedInstanceState
     *            Bundle
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user);

        datasource = new UsersDataSource(this);
        datasource.open();

        List<User> values = datasource.getAllUsers();

        // Use the SimpleCursorAdapter to show the
        // elements in a ListView
        ArrayAdapter<User> adapter = new ArrayAdapter<User>(this,
                android.R.layout.simple_list_item_1, values);
        setListAdapter(adapter);
    }

    /**
     * onClick Method - Will be called via the onClick attribute of the buttons
     * in main.xml.
     * 
     * @param view
     */
    public void onClick(View view) {
        @SuppressWarnings("unchecked")
        ArrayAdapter<User> adapter = (ArrayAdapter<User>) getListAdapter();
        User user = null;
        User user1 = null;
        EditText nameField = (EditText) findViewById(R.id.editTextName);
        String name = nameField.getText().toString();
        switch (view.getId()) {
        case R.id.add:
            user = datasource.createUser(name);
            adapter.add(user);
            break;
        case R.id.delete:
            if (getListAdapter().getCount() > 0) {
                int number = Integer.parseInt(name);
                user = (User) getListAdapter().getItem(number);
                datasource.deleteUser(user);
                adapter.remove(user);
            }
            break;
        case R.id.modify:
            Intent mainIntent = new Intent().setClass(
                    TestDatabaseActivity.this, ModifyUserActivity.class);
            startActivity(mainIntent);
            break;
        }
        adapter.notifyDataSetChanged();
    }

    /**
     * onResume method.
     */
    @Override
    protected void onResume() {
        datasource.open();
        super.onResume();
    }

    /**
     * onPause method.
     */
    @Override
    protected void onPause() {
        datasource.close();
        super.onPause();
    }

}