package com.nxball.game;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Formatter;

import org.anddev.andengine.util.Debug;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.nxball.game.multiplayer.MultiplayerActivity;

import android.app.Activity;
import android.content.Intent;
import android.net.ParseException;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

public class AfterMatchActivity extends Activity {
	
	JSONArray jArray;
	String result = null;
	InputStream is = null;
	StringBuilder sb=null;
	String key = "";
	
	int game_time = 0;
	int score_a = 0;
	int score_b = 0;
	ArrayList<String> imeis;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    Bundle extras = getIntent().getExtras();
	    setContentView(R.layout.aftermatch);
	    TextView t = (TextView)findViewById(R.id.textView1);
	    
	    score_a = extras.getInt("team_a_score");
	    score_b = extras.getInt("team_b_score");
	    game_time = extras.getInt("game_time");
	    imeis = new ArrayList<String>();
	    if (extras.getStringArrayList("imeis") != null) {
	    	imeis = extras.getStringArrayList("imeis");
	    }
	    //tutaj trzeba pobrac imei graczy, na razie na sztywno sa wpisane poscie
	    //...
	    t.setText(String.format("%d:%d",
                score_a, score_b));
	    
	    final Button button = (Button) findViewById(R.id.button_menu);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                CheckBox send = (CheckBox) findViewById(R.id.checkBox_send);
                if (send.isChecked()) {
                	sendMatchData();
                }
                Intent mainIntent = new Intent().setClass(
                        AfterMatchActivity.this,
                        MenuScrollerActivity.class);
                startActivity(mainIntent);
            }
        });
	}
	
	private void sendMatchData()
	{
		DateFormat dateFormat = new SimpleDateFormat("d, MM yyyy");
		Date date = new Date();
		key = toSha1("NXBall" + dateFormat.format(date));
		executeQuery();
	}
	
	/*
	 * Helper method for sha1
	 */
	private String byteToHex(final byte[] hash)
	{
	    Formatter formatter = new Formatter();
	    for (byte b : hash)
	    {
	        formatter.format("%02x", b);
	    }
	    return formatter.toString();
	}
	
	/**
	 * Converting string to sha1 hash
	 * 
	 * @param text
	 * @return
	 */
	private String toSha1(String text)
	{
	    String sha1 = "";
	    try
	    {
	        MessageDigest crypt = MessageDigest.getInstance("SHA-1");
	        crypt.reset();
	        crypt.update(text.getBytes("UTF-8"));
	        sha1 = byteToHex(crypt.digest());
	    }
	    catch(NoSuchAlgorithmException e)
	    {
	        e.printStackTrace();
	    }
	    catch(UnsupportedEncodingException e)
	    {
	        e.printStackTrace();
	    }
	    return sha1;
	}
	
	/**
	 * getQueryResult method.
	 */
	private void executeQuery() {
		ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("action","insert_match"));
		nameValuePairs.add(new BasicNameValuePair("key",key));
		nameValuePairs.add(new BasicNameValuePair("blue_score", "" + score_a));
		nameValuePairs.add(new BasicNameValuePair("red_score", "" + score_b));
		nameValuePairs.add(new BasicNameValuePair("game_time", "" + game_time));
		String blue_id = "1234567890";
		String red_id = "0987654321";
		if (imeis.size() >= 2) {
			Debug.i("Biore imei");
			Debug.i(imeis.get(0));
			Debug.i(imeis.get(1));
			blue_id = imeis.get(0);
			red_id = imeis.get(1);
		}
		nameValuePairs.add(new BasicNameValuePair("blue_ids", blue_id));
		nameValuePairs.add(new BasicNameValuePair("red_ids", red_id));
		//nameValuePairs.add(new BasicNameValuePair("blue_nick", "Tester1"));
		//nameValuePairs.add(new BasicNameValuePair("red_nick", "Tester2"));
		//http post
		try{
		     HttpClient httpclient = new DefaultHttpClient();
		     HttpPost httppost = new HttpPost("http://nxb.kubas1989.mydevil.net/interface.php");
		     httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
		     HttpResponse response = httpclient.execute(httppost);
		     HttpEntity entity = response.getEntity();
		     is = entity.getContent();
		     }catch(Exception e){
		         Log.e("log_tag", "Error in http connection"+e.toString());
		    }
	}

}
