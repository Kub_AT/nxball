package com.nxball.game;

import java.util.ArrayList;
import java.util.List;

import org.anddev.andengine.engine.Engine;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.engine.options.EngineOptions;
import org.anddev.andengine.engine.options.EngineOptions.ScreenOrientation;
import org.anddev.andengine.engine.options.resolutionpolicy.FillResolutionPolicy;
import org.anddev.andengine.entity.primitive.Rectangle;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.entity.scene.Scene.IOnSceneTouchListener;
import org.anddev.andengine.entity.scene.background.ColorBackground;
import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.entity.util.FPSLogger;
import org.anddev.andengine.input.touch.TouchEvent;
import org.anddev.andengine.input.touch.detector.ClickDetector;
import org.anddev.andengine.input.touch.detector.ClickDetector.IClickDetectorListener;
import org.anddev.andengine.input.touch.detector.ScrollDetector;
import org.anddev.andengine.input.touch.detector.ScrollDetector.IScrollDetectorListener;
import org.anddev.andengine.input.touch.detector.SurfaceScrollDetector;
import org.anddev.andengine.opengl.texture.TextureOptions;
import org.anddev.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.anddev.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.ui.activity.BaseGameActivity;

import android.content.Intent;
import android.webkit.WebView;

import com.nxball.game.multiplayer.MultiplayerActivity;


/**
 * MenuScrollerActivity Class
 * @author Hubert Okuniewski
 * @email hokuniewski@gmail.com
 *
 */
public class MenuScrollerActivity extends BaseGameActivity implements IScrollDetectorListener, IOnSceneTouchListener, IClickDetectorListener {
   
    // ===========================================================
    // Constants
    // ===========================================================
    protected static int CAMERA_WIDTH = 480;
    protected static int CAMERA_HEIGHT = 320;
    protected static int FONT_SIZE = 24;
    protected static int PADDING = 50;
    protected static int MENUITEMS = 7;
        
    // ===========================================================
    // Fields
    // ===========================================================
    private Scene mScene;
    private Camera mCamera;
 
	private BitmapTextureAtlas mMenuTextureAtlas;
	private BitmapTextureAtlas mMenuTextureAtlas2; 
	private TextureRegion mMenuLeftTextureRegion;
	private TextureRegion mMenuRightTextureRegion;
		
	private BitmapTextureAtlas mMenuTextureScroll;
	private TextureRegion mMenuScrollTextureRegion;
		
	private BitmapTextureAtlas mMenuTextureBackground;
	private TextureRegion mMenuBackgroundTextureRegion;
		
	private Sprite menuleft;
	private Sprite menuright;
		 
	// Scrolling
	private SurfaceScrollDetector mScrollDetector;
	private ClickDetector mClickDetector;
 
    private float mMinX = 0;
    private float mMaxX = 0;
    private float mCurrentX = 0;
    private int iItemClicked = -1;
        
    //private Rectangle scrollBar;
    private Sprite scrollBar;
    private Sprite background;
    private List<TextureRegion> columns = new ArrayList<TextureRegion>();
    private List<TextureRegion> fields = new ArrayList<TextureRegion>();
    
    
    WebView mWebView;

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================
 
    /**
     * onLoadResources Method.
     */
    @Override
    public void onLoadResources() {
        // Paths
        BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/");

        //Images for the menu
        for (int i = 1; i < MENUITEMS; i++) {
        	BitmapTextureAtlas mMenuBitmapTextureAtlas = new BitmapTextureAtlas(256,256, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
    		TextureRegion mMenuTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(mMenuBitmapTextureAtlas, this, "menu"+i+".png", 0, 128);
        	
        	this.mEngine.getTextureManager().loadTexture(mMenuBitmapTextureAtlas);
        	columns.add(mMenuTextureRegion);
        }
        //Textures for menu arrows
        this.mMenuTextureAtlas = new BitmapTextureAtlas(64,64, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.mMenuRightTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(mMenuTextureAtlas, this, "menu_right.png",0, 0);
        this.mEngine.getTextureManager().loadTexture(mMenuTextureAtlas);
        
        this.mMenuTextureAtlas2 = new BitmapTextureAtlas(64,64, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.mMenuLeftTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(mMenuTextureAtlas2, this, "menu_left.png",0 , 0);
        this.mEngine.getTextureManager().loadTexture(mMenuTextureAtlas2);
        
        //Textures for menu scroll
        this.mMenuTextureScroll = new BitmapTextureAtlas(256,64, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.mMenuScrollTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(mMenuTextureScroll, this, "splash.png",0, 0);
        this.mEngine.getTextureManager().loadTexture(mMenuTextureScroll);
      
        // Background texture
        this.mMenuTextureBackground = new BitmapTextureAtlas(512,512, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.mMenuBackgroundTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(mMenuTextureBackground, this, "field2.png",0, 0);
        this.mEngine.getTextureManager().loadTexture(mMenuTextureBackground);
        }
 
    /**
     * onLoadEngine Method.
     * @return engine Engine
     */
    @Override
    public Engine onLoadEngine() {
    	this.mCamera = new Camera(0, 0, CAMERA_WIDTH, CAMERA_HEIGHT);
    	final EngineOptions engineOptions = new EngineOptions(true, ScreenOrientation.LANDSCAPE, new FillResolutionPolicy(), this.mCamera);
    	engineOptions.getTouchOptions().setRunOnUpdateThread(true);
    	
        final Engine engine = new Engine(engineOptions);
    	return engine;
    }
 
    /**
     * onLoadScene Method
     * @return mScene Scene
     */
    @Override
    public Scene onLoadScene() {
    	this.mEngine.registerUpdateHandler(new FPSLogger());
    	
    	this.mScene = new Scene();
        //this.mScene.setBackground(new ColorBackground((float) 0.5, (float) 0.55, (float) 0.88));

            this.mScrollDetector = new SurfaceScrollDetector(this);
            this.mClickDetector = new ClickDetector(this);
            this.mScene.setOnSceneTouchListener(this);
            this.mScene.setTouchAreaBindingEnabled(true);
            this.mScene.setOnSceneTouchListenerBindingEnabled(true);
            CreateMenuBoxes();
 
            return this.mScene;
        }

    /**
     * onSceneTouchEvent Method.
     * @param pScene Scene
     * @param pSceneTouchEvent TouchEvent
     * @return True Boolean
     */
    @Override
    public boolean onSceneTouchEvent(final Scene pScene, final TouchEvent pSceneTouchEvent) {
        this.mClickDetector.onTouchEvent(pSceneTouchEvent);
        this.mScrollDetector.onTouchEvent(pSceneTouchEvent);
        return true;
    }

    /**
     * onScroll Method.
     * @param pScollDetector ScrollDetector
     * @param pTouchEvent TouchEvent 
     * @param pDistanceX float
     * @param pDistanceY float
     * @return
     */
    @Override
    public void onScroll(final ScrollDetector pScollDetector, final TouchEvent pTouchEvent, final float pDistanceX, final float pDistanceY) {
		//Disable the menu arrows left and right (15px padding)
    	if(mCamera.getMinX() <= 15)
         	menuleft.setVisible(false);
         else
         	menuleft.setVisible(true);
    	 
    	 if(mCamera.getMinX() > mMaxX-15)
             menuright.setVisible(false);
         else
        	 menuright.setVisible(true);
         	
        //Return if ends are reached
        if ( ((mCurrentX - pDistanceX) < mMinX) ) {                	
            return;
        }else if((mCurrentX - pDistanceX) > mMaxX) {
        	return;
        }
        
        //Center camera to the current point
        this.mCamera.offsetCenter(-pDistanceX,0 );
        mCurrentX -= pDistanceX;
        	
        //Set the scrollbar with the camera
        float tempX =mCamera.getCenterX()-CAMERA_WIDTH/2;
        // add the % part to the position
        tempX+= (tempX/(mMaxX+CAMERA_WIDTH))*CAMERA_WIDTH;  
        //set the position
        scrollBar.setPosition(tempX, scrollBar.getY());
        background.setPosition(mCamera.getCenterX()- CAMERA_WIDTH/2, background.getY());
        
        //set the arrows for left and right
        menuright.setPosition(mCamera.getCenterX()+CAMERA_WIDTH/2-menuright.getWidth(),menuright.getY());
        menuleft.setPosition(mCamera.getCenterX()-CAMERA_WIDTH/2,menuleft.getY());
        
        //Because Camera can have negativ X values, so set to 0
        	if(this.mCamera.getMinX() < 0) {
        		this.mCamera.offsetCenter(0,0 );
        		mCurrentX=0;
        	}
        }
 
    /**
     * onClick Method.
     * @param pClickDetector ClickDetector
     * @param pTouchEvent TouchEvent
     */
        @Override
        public void onClick(ClickDetector pClickDetector, TouchEvent pTouchEvent) {
                loadLevel(iItemClicked);
        };
 
        // ===========================================================
    // Methods
    // ===========================================================
    
    /**
     * CreateMenuBoxes Method.
     */
    private void CreateMenuBoxes() {
         int spriteX = PADDING;
         int spriteY = PADDING;
    	 
    	 //current item counter
         int iItem = 1;
         
         background = new Sprite(-20,-55,mMenuBackgroundTextureRegion);
    	 this.mScene.attachChild(background);
    	 
    	 for (int x = 0; x < columns.size(); x++) {
    		 if((x%2) != 0){
    			 spriteY = PADDING + 50;
    		 }
    		 else spriteY = PADDING;
    		 
    		 //On Touch, save the clicked item in case it's a click and not a scroll.
                 final int itemToLoad = iItem;
        		 
        		 Sprite sprite = new Sprite(spriteX,spriteY,columns.get(x)){
        			 
        			 public boolean onAreaTouched(final TouchEvent pSceneTouchEvent, final float pTouchAreaLocalX, final float pTouchAreaLocalY) {
                         iItemClicked = itemToLoad;
                         return false;
        			 }        			 
        		 }; 
        		 
        		 iItem++;
        		 this.mScene.attachChild(sprite);        		 
        		 this.mScene.registerTouchArea(sprite);        		 
   
        		 spriteX += 20 + PADDING+sprite.getWidth();
			}
        	
        	 mMaxX = spriteX - CAMERA_WIDTH;
        
        	 scrollBar = new Sprite(0, CAMERA_HEIGHT-45, mMenuScrollTextureRegion);
        	 // this.mScene.attachChild(scrollBar);
        	 
        	 menuleft = new Sprite(0,CAMERA_HEIGHT/2-mMenuLeftTextureRegion.getHeight()/2 + 30,mMenuLeftTextureRegion);
        	 menuright = new Sprite(CAMERA_WIDTH-mMenuRightTextureRegion.getWidth(),CAMERA_HEIGHT/2-mMenuRightTextureRegion.getHeight()/2 + 30,mMenuRightTextureRegion);
        	 this.mScene.attachChild(menuright);
        	 menuleft.setVisible(false);
        	 this.mScene.attachChild(menuleft);
        }

        /**
     * onLoadComplete Method.
     */
        @Override
        public void onLoadComplete() {
 
        }
 
        /**
     * loadLevel Method. Here is where you call the item load.
     * @param iLevel
     */
        private void loadLevel(final int iLevel) {
            if (iLevel != -1) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                		//if ( iLevel == 1) {
                			//Intent mainIntent = new Intent().setClass(MenuScrollerActivity.this, MultiplayerActivity.class);
            				//startActivity(mainIntent);
                		//} 
                		if ( iLevel == 1) {
                			Intent mainIntent = new Intent().setClass(MenuScrollerActivity.this, MultiplayerActivity.class);
            				startActivity(mainIntent);
                		} if ( iLevel == 2) {
                			Intent mainIntent = new Intent().setClass(MenuScrollerActivity.this, ScoreWebActivity.class);
            				startActivity(mainIntent);
                		}
                		if ( iLevel == 3) {
                			Intent mainIntent = new Intent().setClass(MenuScrollerActivity.this, CommonActivity.class);
            				startActivity(mainIntent);
                		}
                		if ( iLevel == 4) {
                			Intent mainIntent = new Intent().setClass(MenuScrollerActivity.this, WebActivity.class);
            				startActivity(mainIntent);
                		}
						if ( iLevel == 5) {
                			Intent mainIntent = new Intent().setClass(MenuScrollerActivity.this, ManualActivity.class);
            				startActivity(mainIntent);
                		}
                		if ( iLevel == 6) {
                			finish();
                		}
 
                		iItemClicked = -1;
                    }
                });
            }
        }
}
 