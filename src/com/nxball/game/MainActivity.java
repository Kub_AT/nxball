package com.nxball.game;

//CHECKSTYLE:OFF

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.microedition.khronos.opengles.GL10;

import org.anddev.andengine.engine.Engine;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.engine.camera.hud.controls.AnalogOnScreenControl;
import org.anddev.andengine.engine.camera.hud.controls.AnalogOnScreenControl.IAnalogOnScreenControlListener;
import org.anddev.andengine.engine.camera.hud.controls.BaseOnScreenControl;
import org.anddev.andengine.engine.handler.IUpdateHandler;
import org.anddev.andengine.engine.handler.physics.PhysicsHandler;
import org.anddev.andengine.engine.handler.timer.ITimerCallback;
import org.anddev.andengine.engine.handler.timer.TimerHandler;
import org.anddev.andengine.engine.options.EngineOptions;
import org.anddev.andengine.engine.options.EngineOptions.ScreenOrientation;
import org.anddev.andengine.engine.options.resolutionpolicy.RatioResolutionPolicy;
import org.anddev.andengine.entity.modifier.AlphaModifier;
import org.anddev.andengine.entity.modifier.IEntityModifier;
import org.anddev.andengine.entity.modifier.ParallelEntityModifier;
import org.anddev.andengine.entity.modifier.RotationModifier;
import org.anddev.andengine.entity.modifier.ScaleModifier;
import org.anddev.andengine.entity.modifier.SequenceEntityModifier;
import org.anddev.andengine.entity.primitive.Rectangle;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.entity.scene.background.ColorBackground;
import org.anddev.andengine.entity.shape.Shape;
import org.anddev.andengine.entity.sprite.AnimatedSprite;
import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.entity.util.FPSCounter;
import org.anddev.andengine.entity.util.FPSLogger;
import org.anddev.andengine.extension.input.touch.controller.MultiTouch;
import org.anddev.andengine.extension.input.touch.controller.MultiTouchController;
import org.anddev.andengine.extension.input.touch.exception.MultiTouchException;
import org.anddev.andengine.extension.physics.box2d.FixedStepPhysicsWorld;
import org.anddev.andengine.extension.physics.box2d.PhysicsConnector;
import org.anddev.andengine.extension.physics.box2d.PhysicsFactory;
import org.anddev.andengine.extension.physics.box2d.PhysicsWorld;
import org.anddev.andengine.extension.physics.box2d.util.Vector2Pool;
import org.anddev.andengine.opengl.font.Font;
import org.anddev.andengine.opengl.texture.TextureOptions;
import org.anddev.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.anddev.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.opengl.texture.region.TiledTextureRegion;
import org.anddev.andengine.ui.activity.BaseGameActivity;
import org.anddev.andengine.util.HorizontalAlign;
import org.anddev.andengine.entity.text.ChangeableText;
import org.anddev.andengine.entity.text.Text;
import org.anddev.andengine.entity.text.TickerText;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.text.format.DateFormat;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import android.widget.Toast;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.FixtureDef;

//CHECKSTYLE:ON

/**
 * MainActivity class.
 * 
 * @version 0.2
 * @see BaseGameActivity
 */
public class MainActivity extends BaseGameActivity {
    /*
     * Variables.
     */

    private Integer i = new Integer(0);
    /**
     * CAMERA_WIDTH - camerta width.
     */
    private PhysicsWorld mPhysicsWorld;
    public static final int GAME_WIDTH = 480;
    public static final int GAME_WIDTH_HALF = GAME_WIDTH / 2;
    public static final int GAME_HEIGHT = 320;
    public static final int GAME_HEIGHT_HALF = GAME_HEIGHT / 2;

    public static final int PADDLE_WIDTH = 80;
    public static final int PADDLE_WIDTH_HALF = PADDLE_WIDTH / 2;
    public static final int PADDLE_HEIGHT = 20;
    public static final int PADDLE_HEIGHT_HALF = PADDLE_HEIGHT / 2;

    private static final int CAMERA_WIDTH = 480;

    /**
     * CAMERA_HEIGHT - camerta height.
     */
    private static final int CAMERA_HEIGHT = 320;

    /**
     * Kamera.
     */
    private Camera mCamera;

    public static final short CATEGORYBIT_WALL = 1;
    public static final short CATEGORYBIT_WALL2 = 2;
    public static final short CATEGORYBIT_FACE = 4;
    public static final short CATEGORYBIT_SOCCER = 8;

    public static final short MASKBITS_WALL = CATEGORYBIT_WALL
            + CATEGORYBIT_FACE + CATEGORYBIT_SOCCER;
    public static final short MASKBITS_WALL2 = CATEGORYBIT_WALL2
            + CATEGORYBIT_SOCCER;
    public static final short MASKBITS_FACE = CATEGORYBIT_WALL
            + CATEGORYBIT_FACE + CATEGORYBIT_SOCCER;
    public static final short MASKBITS_SOCCER = CATEGORYBIT_WALL
            + CATEGORYBIT_WALL2 + CATEGORYBIT_FACE;

    /**
     * Bitmap Textur.
     */
    private BitmapTextureAtlas mBitmapTextureAtlas;

    private BitmapTextureAtlas mBoiskoTextureAtlas;

    /**
     * Face Texture Region.
     */
    private TiledTextureRegion mSoccerTextureRegion;

    private BitmapTextureAtlas mSoccerTextureAtlas;

    private TextureRegion mSlupekTextureRegion;

    private BitmapTextureAtlas mSlupekTextureAtlas;

    private TextureRegion mBoiskoTextureRegion;

    private BitmapTextureAtlas mBoisko2TextureAtlas;

    private TextureRegion mBoisko2TextureRegion;

    private BitmapTextureAtlas mBoisko3TextureAtlas;

    private TextureRegion mBoisko3TextureRegion;

    private TiledTextureRegion mFaceTextureRegion;

    /**
     * OnScreenControlTexture.
     */
    private BitmapTextureAtlas mOnScreenControlTexture;

    /**
     * OnScreenControlBaseTextureRegion.
     */
    private TextureRegion mOnScreenControlBaseTextureRegion;

    /**
     * OnScreenControlKnobTextureRegion.
     */
    private TextureRegion mOnScreenControlKnobTextureRegion;

    /**
     * PlaceOnScreenControlsAtDifferentVerticalLocations.
     */
    private boolean mPlaceOnScreenControlsAtDifferentVerticalLocations = false;

    /**
     * OnScreenControlKickTexture.
     */
    private BitmapTextureAtlas mOnScreenControlKickTexture;

    /**
     * OnScreenControlKickTextureRegion.
     */
    private TextureRegion mOnScreenControlKickTextureRegion;

    private Body mCarBody;
    private Body mCarBody2;

    private Scene scene;

    /**
     * For loading settings;
     */
    private SharedPreferences sharedPrefs;

    /**
     * Jak szybko pilka sie zatrzymuje.
     */
    private final float ballLinearDamping = 0.7f;
    
    /**
     * Jak szybko gracz sie porusza.
     */
    private final float playerVelocity = 3.0f;
    
    /**
     * Czas
     */
    private BitmapTextureAtlas mFontTexture;
    private Font mFont;
    private int seconds = 0;
    private int minutes = 0;
    
    /**
     * Wynik
     */
    private BitmapTextureAtlas sFontTexture;
    private Font sFont;
    private int team_a_score = 0;
    private int team_b_score = 0;

    /*
     * Methods.
     */

    /*
     * @Override public void onCreate(Bundle savedInstanceState) {
     * super.onCreate(savedInstanceState); //setContentView(R.layout.main);
     * 
     * 
     * }
     */
    /**
     * onLoadEngine Method.
     * 
     * @return engine
     */
    public final Engine onLoadEngine() {
        sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        this.mCamera = new Camera(0, 0, CAMERA_WIDTH, CAMERA_HEIGHT);
        final Engine engine = new Engine(new EngineOptions(true,
                ScreenOrientation.LANDSCAPE, new RatioResolutionPolicy(
                        CAMERA_WIDTH, CAMERA_HEIGHT), this.mCamera));

        return engine;
    }

    /**
     * onLoadResources method.
     */
    public final void onLoadResources() {
        BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/");

        final int t4x = 4;
        final int t16x = 16;
        final int t32x = 32;
        final int t64x = 64;
        final int t128x = 128;
        final int t256x = 256;
        final int t512x = 512;

        // do pomiaru czasu
        this.mFontTexture = new BitmapTextureAtlas(256, 256,
                TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.mFont = new Font(this.mFontTexture, Typeface.create(
                Typeface.DEFAULT, Typeface.BOLD), 24, true, Color.BLACK);
        this.mEngine.getTextureManager().loadTexture(this.mFontTexture);
        this.getFontManager().loadFont(this.mFont);
        //

        // do zliczania punkt�w
        this.sFontTexture = new BitmapTextureAtlas(256, 256,
                TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.sFont = new Font(this.sFontTexture, Typeface.create(
                Typeface.DEFAULT, Typeface.BOLD), 24, true, Color.BLACK);
        this.mEngine.getTextureManager().loadTexture(this.sFontTexture);
        this.getFontManager().loadFont(this.sFont);
        //

        this.mBitmapTextureAtlas = new BitmapTextureAtlas(t64x, t64x,
                TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.mFaceTextureRegion = BitmapTextureAtlasTextureRegionFactory
                .createTiledFromAsset(this.mBitmapTextureAtlas, this,
                        "ball.png", 0, 0, 1, 1);

        this.mSoccerTextureAtlas = new BitmapTextureAtlas(t16x, t16x,
                TextureOptions.BILINEAR_PREMULTIPLYALPHA); // mniejsza pilka
        this.mSoccerTextureRegion = BitmapTextureAtlasTextureRegionFactory
                .createTiledFromAsset(this.mSoccerTextureAtlas, this,
                        "soccer2.png", 0, 0, 1, 1);

        // this.mSoccerTextureAtlas = new BitmapTextureAtlas(t32x, t32x,
        // TextureOptions.BILINEAR_PREMULTIPLYALPHA); // wieksza pilka
        // this.mSoccerTextureRegion =
        // BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mSoccerTextureAtlas,
        // this, "soccer.png", 0, 0, 1, 1);

        this.mBoiskoTextureAtlas = new BitmapTextureAtlas(t128x, t256x,
                TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.mBoiskoTextureRegion = BitmapTextureAtlasTextureRegionFactory
                .createFromAsset(this.mBoiskoTextureAtlas, this, "boisko1.png",
                        0, 0);

        this.mBoisko2TextureAtlas = new BitmapTextureAtlas(t128x, t256x,
                TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.mBoisko2TextureRegion = BitmapTextureAtlasTextureRegionFactory
                .createFromAsset(this.mBoisko2TextureAtlas, this,
                        "boisko2.png", 0, 0);

        this.mBoisko3TextureAtlas = new BitmapTextureAtlas(t128x, t256x,
                TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.mBoisko3TextureRegion = BitmapTextureAtlasTextureRegionFactory
                .createFromAsset(this.mBoisko3TextureAtlas, this,
                        "boisko3.png", 0, 0);

        this.mSlupekTextureAtlas = new BitmapTextureAtlas(t4x, t4x,
                TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.mSlupekTextureRegion = BitmapTextureAtlasTextureRegionFactory
                .createFromAsset(this.mSlupekTextureAtlas, this, "slupek.png",
                        0, 0);

        this.mOnScreenControlTexture = new BitmapTextureAtlas(t256x, t128x,
                TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.mOnScreenControlBaseTextureRegion = BitmapTextureAtlasTextureRegionFactory
                .createFromAsset(this.mOnScreenControlTexture, this,
                        "onscreen_control_base.png", 0, 0);
        this.mOnScreenControlKnobTextureRegion = BitmapTextureAtlasTextureRegionFactory
                .createFromAsset(this.mOnScreenControlTexture, this,
                        "onscreen_control_knob.png", t128x, 0);

        this.mOnScreenControlKickTexture = new BitmapTextureAtlas(t128x, t64x,
                TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.mOnScreenControlKickTextureRegion = BitmapTextureAtlasTextureRegionFactory
                .createFromAsset(this.mOnScreenControlKickTexture, this,
                        "onscreen_control_kick.png", 0, 0);

        this.mEngine.getTextureManager().loadTextures(this.mBitmapTextureAtlas,
                this.mOnScreenControlTexture, this.mOnScreenControlKickTexture,
                this.mBoiskoTextureAtlas, this.mBoisko2TextureAtlas,
                this.mBoisko3TextureAtlas, this.mSoccerTextureAtlas); // this.mBackgroundTexture
    }

    /**
     * onLoadScene method.
     * 
     * @return scene
     */
    public final Scene onLoadScene() {
        /**
         * RGB
         */
        /**
         * pomiar czasu
         */

        this.mPhysicsWorld = new FixedStepPhysicsWorld(30, new Vector2(0, 0),
                false, 8, 1);

        final float backgroundRed = 0.572f;
        final float backgroundGreen = 0.749f;
        final float backgroundBlue = 0.4156f;

        /**
         * Velocity.
         */
        final float velocity = 100;

        /**
         * Alpha.
         */
        final float alpha = 0.5f;

        this.mEngine.registerUpdateHandler(new FPSLogger());

        this.scene = new Scene();
        scene.setBackground(new ColorBackground(backgroundRed, backgroundGreen,
                backgroundBlue));
        // scene.attachChild(new Line(-GAME_WIDTH_HALF + 1, -GAME_HEIGHT_HALF,
        // -GAME_WIDTH_HALF + 1, GAME_HEIGHT_HALF)); // Left
        // scene.attachChild(new Line(GAME_WIDTH_HALF, -GAME_HEIGHT_HALF,
        // GAME_WIDTH_HALF, GAME_HEIGHT_HALF)); // Right
        // scene.attachChild(new Line(-GAME_WIDTH_HALF, -GAME_HEIGHT_HALF + 1,
        // GAME_WIDTH_HALF , -GAME_HEIGHT_HALF + 1)); // Top
        // scene.attachChild(new Line(-GAME_WIDTH_HALF, GAME_HEIGHT_HALF,
        // GAME_WIDTH_HALF, GAME_HEIGHT_HALF)); // Bottom

        // scene.attachChild(new Line(0, -GAME_HEIGHT_HALF, 0,
        // GAME_HEIGHT_HALF)); // Middle

        final int centerX = (CAMERA_WIDTH - this.mFaceTextureRegion.getWidth()) / 2;
        final int centerY = (CAMERA_HEIGHT - this.mFaceTextureRegion
                .getHeight()) / 2;

        final Sprite boisko = new Sprite(CAMERA_WIDTH / 2
                - (this.mBoiskoTextureRegion.getWidth() / 2)
                - this.mBoiskoTextureRegion.getWidth(),
                0 + this.mFaceTextureRegion.getWidth(),
                this.mBoiskoTextureRegion);
        final Sprite boisko2 = new Sprite(CAMERA_WIDTH / 2
                - (this.mBoiskoTextureRegion.getWidth() / 2),
                0 + this.mFaceTextureRegion.getWidth(),
                this.mBoisko2TextureRegion);
        final Sprite boisko3 = new Sprite(CAMERA_WIDTH / 2
                + (this.mBoiskoTextureRegion.getWidth() / 2),
                0 + this.mFaceTextureRegion.getWidth(),
                this.mBoisko3TextureRegion);

        // final Sprite slupek = new
        // Sprite(CAMERA_WIDTH/2-(this.mBoiskoTextureRegion.getWidth()/2)-this.mBoiskoTextureRegion.getWidth(),(float)
        // (0 + this.mFaceTextureRegion.getWidth()
        // +0.30*this.mBoiskoTextureRegion.getHeight()),
        // this.mSlupekTextureRegion);
        final Shape slupek = new Rectangle(
                CAMERA_WIDTH / 2 - (this.mBoiskoTextureRegion.getWidth() / 2)
                        - this.mBoiskoTextureRegion.getWidth(),
                (float) (0 + this.mFaceTextureRegion.getWidth() + 0.30 * this.mBoiskoTextureRegion
                        .getHeight()), 2, 2);
        final Shape slupek2 = new Rectangle(
                CAMERA_WIDTH / 2 + (this.mBoiskoTextureRegion.getWidth() / 2)
                        + this.mBoiskoTextureRegion.getWidth() - 2,
                (float) (0 + this.mFaceTextureRegion.getWidth() + 0.30 * this.mBoiskoTextureRegion
                        .getHeight()), 2, 2);
        final Shape slupek3 = new Rectangle(
                CAMERA_WIDTH / 2 - (this.mBoiskoTextureRegion.getWidth() / 2)
                        - this.mBoiskoTextureRegion.getWidth(),
                (float) (0 + this.mFaceTextureRegion.getWidth() + 0.6914 * this.mBoiskoTextureRegion
                        .getHeight()), 2, 2);
        final Shape slupek4 = new Rectangle(
                CAMERA_WIDTH / 2 + (this.mBoiskoTextureRegion.getWidth() / 2)
                        + this.mBoiskoTextureRegion.getWidth() - 2,
                (float) (0 + this.mFaceTextureRegion.getWidth() + 0.6914 * this.mBoiskoTextureRegion
                        .getHeight()), 2, 2);

        final Sprite kickButton = new Sprite(0, CAMERA_HEIGHT
                - this.mOnScreenControlKickTextureRegion.getHeight(),
                this.mOnScreenControlKickTextureRegion);
        final FPSCounter fpsCounter = new FPSCounter();
        this.mEngine.registerUpdateHandler(fpsCounter);

        // ---------------------------------------------Time measure method
        // ---------------------------------------------------

        final ChangeableText elapsedText = new ChangeableText(10, 5,
                this.mFont, "00:00", "xxxxx".length());
        mEngine.registerUpdateHandler(new TimerHandler(1f, true,
                new ITimerCallback() {
                    @Override
                    public void onTimePassed(final TimerHandler pTimerHandler) {
                        seconds += 1;
                        if (seconds == 60) {
                            seconds = 0;
                            minutes += 1;
                        }
                        elapsedText.setText(String.format("%02d:%02d", minutes,
                                seconds));
                        if (minutes == 10) {
                            Toast.makeText(getApplicationContext(),
                                    "End of game!", Toast.LENGTH_SHORT).show();
                            Intent mainIntent = new Intent().setClass(
                                    MainActivity.this,
                                    MenuScrollerActivity.class);
                            startActivity(mainIntent);
                        }
                    }
                }));
        // ----------------------------------------------------------------------------------------------------------------------

        //Zliczanie punktow
        final ChangeableText scoreText = new ChangeableText(
                (CAMERA_WIDTH / 2 - 10), 5, this.mFont, "0:0", "xxx".length());
        mEngine.registerUpdateHandler(new TimerHandler(1f, true,
                new ITimerCallback() {
                    @Override
                    public void onTimePassed(final TimerHandler pTimerHandler) {
                        // wykrycie kolizji
                        scoreText.setText(String.format("%01d:%01d",
                                team_a_score, team_b_score));

                    }
                }));
        // -------------------------------------------------------------------------------------------------------------------------

        scene.attachChild(boisko);
        scene.attachChild(boisko2);
        scene.attachChild(boisko3);
        scene.attachChild(slupek);
        scene.attachChild(slupek2);
        scene.attachChild(slupek3);
        scene.attachChild(slupek4);
        scene.attachChild(kickButton);
        //czas
        scene.attachChild(elapsedText);
        scene.attachChild(scoreText);

        final AnimatedSprite face = new AnimatedSprite(centerX, centerY,
                this.mFaceTextureRegion);
        final AnimatedSprite soccer = new AnimatedSprite(centerX - 30,
                centerY - 30, this.mSoccerTextureRegion);

        final FixtureDef carFixtureDef = PhysicsFactory.createFixtureDef(1,
                0.5f, 0.5f, false, CATEGORYBIT_WALL, MASKBITS_WALL, (short) 0);
        final FixtureDef carFixtureDef2 = PhysicsFactory
                .createFixtureDef(1, 0.5f, 0.5f, false, CATEGORYBIT_WALL2,
                        MASKBITS_WALL2, (short) 0);
        final FixtureDef carFixtureDef3 = PhysicsFactory.createFixtureDef(1,
                0.5f, 0.5f, false, CATEGORYBIT_FACE, MASKBITS_FACE, (short) 0);
        final FixtureDef carFixtureDef4 = PhysicsFactory.createFixtureDef(1,
                0.5f, 0.5f, false, CATEGORYBIT_SOCCER, MASKBITS_SOCCER,
                (short) 0);

        this.mCarBody = PhysicsFactory.createBoxBody(this.mPhysicsWorld, face,
                BodyType.DynamicBody, carFixtureDef3);
        this.mCarBody2 = PhysicsFactory.createBoxBody(this.mPhysicsWorld,
                soccer, BodyType.DynamicBody, carFixtureDef4);
        this.mCarBody2.setLinearDamping(this.ballLinearDamping);
        this.mCarBody2.setAngularDamping(this.ballLinearDamping);

        this.mPhysicsWorld.registerPhysicsConnector(new PhysicsConnector(face,
                this.mCarBody, true, false));
        this.mPhysicsWorld.registerPhysicsConnector(new PhysicsConnector(
                soccer, this.mCarBody2, true, true));

        final PhysicsHandler physicsHandler = new PhysicsHandler(face);
        face.registerUpdateHandler(physicsHandler);

        final PhysicsHandler physicsHandler2 = new PhysicsHandler(soccer);
        soccer.registerUpdateHandler(physicsHandler2);

        scene.attachChild(face);
        scene.attachChild(soccer);

        /*
         * Getting controller positions from preferences
         */
        final String controllerPos = sharedPrefs.getString(
                "controller_position", "Right");
        int controllerX = CAMERA_WIDTH
                - this.mOnScreenControlBaseTextureRegion.getWidth();
        if (controllerPos.equals("Left")) {
            controllerX = 0;
        }

        /*
         * Velocity control (right). TODO: Pozycjonowanie elementu w inny
         * sposob?
         */
        final int y1base = CAMERA_HEIGHT
                - this.mOnScreenControlBaseTextureRegion.getHeight();
        final int y1 = (this.mPlaceOnScreenControlsAtDifferentVerticalLocations) ? 0
                : y1base;

        final AnalogOnScreenControl velocityOnScreenControl = new AnalogOnScreenControl(
                controllerX, y1, this.mCamera,
                this.mOnScreenControlBaseTextureRegion,
                this.mOnScreenControlKnobTextureRegion, 0.025f,
                new IAnalogOnScreenControlListener() {

                    // final AnalogOnScreenControl analogOnScreenControl = new
                    // AnalogOnScreenControl(0, CAMERA_HEIGHT -
                    // this.mOnScreenControlBaseTextureRegion.getHeight(),
                    // this.mCamera, this.mOnScreenControlBaseTextureRegion,
                    // this.mOnScreenControlKnobTextureRegion, 0.1f, new
                    // IAnalogOnScreenControlListener() {

                    // public void onControlChange(final BaseOnScreenControl
                    // pBaseOnScreenControl, final float pValueX, final float
                    // pValueY) {
                    // physicsHandler.setVelocity(pValueX * velocity, pValueY *
                    // velocity);
                    // }
                    public void onControlChange(
                            final BaseOnScreenControl pBaseOnScreenControl,
                            final float pValueX, final float pValueY) {
                        final Body carBody = MainActivity.this.mCarBody;
                        float velX = 0;
                        float velY = 0;

                        if (pValueX > 0) {
                            velX = MainActivity.this.playerVelocity;
                        } else if (pValueX < 0) {
                            velX = -MainActivity.this.playerVelocity;
                        }
                        if (pValueY > 0) {
                            velY = MainActivity.this.playerVelocity;
                        } else if (pValueY < 0) {
                            velY = -MainActivity.this.playerVelocity;
                        }
                        final Vector2 velocity = Vector2Pool.obtain(velX, velY);
                        carBody.setLinearVelocity(velocity);
                        Vector2Pool.recycle(velocity);

                        // final float rotationInRad =
                        // (float)Math.atan2(-pValueX, pValueY);
                        carBody.setTransform(carBody.getWorldCenter(), 0);

                        // MainActivity.this.mCar.setRotation(MathUtils.radToDeg(rotationInRad));
                    }

                    public void onControlClick(
                            final AnalogOnScreenControl pAnalogOnScreenControl) {
                        /* Nothing. */
                    }
                });
        velocityOnScreenControl.getControlBase().setBlendFunction(
                GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
        velocityOnScreenControl.getControlBase().setAlpha(alpha);

        scene.setChildScene(velocityOnScreenControl);

        scene.registerUpdateHandler(new IUpdateHandler() {
            public void reset() {
            }

            @Override
            public void onUpdate(final float arg0) {
                // TODO Auto-generated method, kolizje itp.
            }

        });
        // krawedzie ekranu
        final Shape ground = new Rectangle(0, CAMERA_HEIGHT - 2, CAMERA_WIDTH,
                2);
        final Shape roof = new Rectangle(0, 0, CAMERA_WIDTH, 2);
        final Shape left = new Rectangle(0, 0, 2, CAMERA_HEIGHT);
        final Shape right = new Rectangle(CAMERA_WIDTH - 2, 0, 2, CAMERA_HEIGHT);
        // final Sprite boisko = new
        // Sprite(CAMERA_WIDTH/2-(this.mBoiskoTextureRegion.getWidth()/2)-this.mBoiskoTextureRegion.getWidth(),0
        // + this.mFaceTextureRegion.getWidth() , this.mBoiskoTextureRegion);

        // krawedzie boiska
        final Shape groundB = new Rectangle(CAMERA_WIDTH / 2
                - (this.mBoiskoTextureRegion.getWidth() / 2)
                - this.mBoiskoTextureRegion.getWidth(), 0
                + this.mFaceTextureRegion.getWidth()
                + this.mBoiskoTextureRegion.getHeight(),
                this.mBoiskoTextureRegion.getWidth() * 3, 1);
        final Shape roofB = new Rectangle(CAMERA_WIDTH / 2
                - (this.mBoiskoTextureRegion.getWidth() / 2)
                - this.mBoiskoTextureRegion.getWidth(),
                0 + this.mFaceTextureRegion.getWidth(),
                this.mBoiskoTextureRegion.getWidth() * 3, 1);
        final Shape leftB1 = new Rectangle(CAMERA_WIDTH / 2
                - (this.mBoiskoTextureRegion.getWidth() / 2)
                - this.mBoiskoTextureRegion.getWidth(),
                0 + this.mFaceTextureRegion.getWidth(), 1,
                (float) (0.30 * this.mBoiskoTextureRegion.getHeight()));
        final Shape rightB1 = new Rectangle(CAMERA_WIDTH / 2
                + (this.mBoiskoTextureRegion.getWidth() / 2)
                + this.mBoiskoTextureRegion.getWidth(),
                0 + this.mFaceTextureRegion.getWidth(), 1,
                (float) (0.30 * this.mBoiskoTextureRegion.getHeight()));
        final Shape leftB2 = new Rectangle(
                CAMERA_WIDTH / 2 - (this.mBoiskoTextureRegion.getWidth() / 2)
                        - this.mBoiskoTextureRegion.getWidth(),
                (float) (0 + this.mFaceTextureRegion.getWidth() + 0.6914 * this.mBoiskoTextureRegion
                        .getHeight()), 1,
                (float) (0.30 * this.mBoiskoTextureRegion.getHeight()));
        final Shape rightB2 = new Rectangle(
                CAMERA_WIDTH / 2 + (this.mBoiskoTextureRegion.getWidth() / 2)
                        + this.mBoiskoTextureRegion.getWidth(),
                (float) (0 + this.mFaceTextureRegion.getWidth() + 0.6914 * this.mBoiskoTextureRegion
                        .getHeight()), 1,
                (float) (0.30 * this.mBoiskoTextureRegion.getHeight()));

        final FixtureDef wallFixtureDef = PhysicsFactory.createFixtureDef(0,
                0.0f, 0.0f);
        PhysicsFactory.createBoxBody(this.mPhysicsWorld, slupek,
                BodyType.StaticBody, carFixtureDef);
        PhysicsFactory.createBoxBody(this.mPhysicsWorld, slupek2,
                BodyType.StaticBody, carFixtureDef);
        PhysicsFactory.createBoxBody(this.mPhysicsWorld, slupek3,
                BodyType.StaticBody, carFixtureDef);
        PhysicsFactory.createBoxBody(this.mPhysicsWorld, slupek4,
                BodyType.StaticBody, carFixtureDef);
        PhysicsFactory.createBoxBody(this.mPhysicsWorld, ground,
                BodyType.StaticBody, carFixtureDef);
        PhysicsFactory.createBoxBody(this.mPhysicsWorld, roof,
                BodyType.StaticBody, carFixtureDef);
        PhysicsFactory.createBoxBody(this.mPhysicsWorld, left,
                BodyType.StaticBody, carFixtureDef);
        PhysicsFactory.createBoxBody(this.mPhysicsWorld, right,
                BodyType.StaticBody, carFixtureDef);

        PhysicsFactory.createBoxBody(this.mPhysicsWorld, groundB,
                BodyType.StaticBody, carFixtureDef2);
        PhysicsFactory.createBoxBody(this.mPhysicsWorld, roofB,
                BodyType.StaticBody, carFixtureDef2);
        PhysicsFactory.createBoxBody(this.mPhysicsWorld, leftB1,
                BodyType.StaticBody, carFixtureDef2);
        PhysicsFactory.createBoxBody(this.mPhysicsWorld, rightB1,
                BodyType.StaticBody, carFixtureDef2);
        PhysicsFactory.createBoxBody(this.mPhysicsWorld, leftB2,
                BodyType.StaticBody, carFixtureDef2);
        PhysicsFactory.createBoxBody(this.mPhysicsWorld, rightB2,
                BodyType.StaticBody, carFixtureDef2);

        this.scene.attachChild(ground);
        this.scene.attachChild(roof);
        this.scene.attachChild(left);
        this.scene.attachChild(right);

        this.scene.attachChild(groundB);
        this.scene.attachChild(roofB);
        this.scene.attachChild(leftB1);
        this.scene.attachChild(rightB1);
        this.scene.attachChild(leftB2);
        this.scene.attachChild(rightB2);

        this.scene.registerUpdateHandler(this.mPhysicsWorld);

        return this.scene;

    }

    /**
     * onLoadComplete method.
     */
    public void onLoadComplete() {
        // onLoadComplete
    }

}
