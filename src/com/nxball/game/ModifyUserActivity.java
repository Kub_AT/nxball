package com.nxball.game;

import java.util.List;
import java.util.Random;

import android.app.ListActivity;
import android.content.Intent;
import android.inputmethodservice.ExtractEditText;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import java.util.*;

/**
 * ModifyUserActivity class.
 *
 */
public class ModifyUserActivity extends ListActivity {
private UsersDataSource datasource;
	
	/**
	 * Called when the activity is first created.
	 * @param savedInstanceState Bundle
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.modify);

		datasource = new UsersDataSource(this);
		datasource.open();

		List<User> values = datasource.getAllUsers();

		// Use the SimpleCursorAdapter to show the
		// elements in a ListView
		ArrayAdapter<User> adapter = new ArrayAdapter<User>(this,
				android.R.layout.simple_list_item_1, values);
		setListAdapter(adapter);
	}

	/**
	 * onClick Method. Will be called via the onClick attribute
	 * of the buttons in main.xml.
	 * @param view
	 */
	public void onClick(View view) {
		@SuppressWarnings("unchecked")
		ArrayAdapter<User> adapter = (ArrayAdapter<User>) getListAdapter();
		User user = null;
		User user1 = null;
		
		EditText oldNumber = (EditText) findViewById(R.id.editOldNumber);
		EditText oldName = (EditText) findViewById(R.id.editOldName);
		EditText newName = (EditText) findViewById(R.id.editNewName);
		EditText newColour = (EditText) findViewById(R.id.editNewColour);
		
		String oldnumber = oldNumber.getText().toString();
		String newname = newName.getText().toString();
		
		switch (view.getId()) {
		case R.id.modify:
			if (getListAdapter().getCount() > 0) { 
				int number = Integer.parseInt(oldnumber);
				user = (User) getListAdapter().getItem(number);			
				datasource.deleteUser(user);
				adapter.remove(user);
			}
			user1 = datasource.createUser(newname);
			adapter.add(user1);		
			break;
			
		case R.id.backtomenu:
			Intent mainIntent = new Intent().setClass(ModifyUserActivity.this, MenuScrollerActivity.class);
			startActivity(mainIntent);
			break;
		}
		
		adapter.notifyDataSetChanged();
	}

	/**
	 * onResume Method.
	 */
	@Override
	protected void onResume() {
		datasource.open();
		super.onResume();
	}

	/**
	 * onPause Method.
	 */
	@Override
	protected void onPause() {
		datasource.close();
		super.onPause();
	}
}
