package com.nxball.game;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.text.format.Time;
import android.view.View;
import android.widget.DatePicker;
import android.widget.Toast;

/**
 * ManualActivity Class.
 * 
 */
public class ManualActivity extends Activity {

    private static final int MY_SUPER_SIMPLE_DIALOG_ID = 0;
    private static final int MY_SIMPLE_DIALOG_ID = 1;
    private static final int MY_DIALOG_ID = 2;
    private static final int moving = 3;
    private static final int set_up_server = 4;
    private static final int connect_to_server = 5;
    private static final int add_user = 6;
    private static final int delete_user = 7;
    private static final int modify_user = 8;
    private static final int check_stats = 9;
    private static final int www = 10;
    private static final int exit = 11;

    private Date mCurrentTime;

    /**
     * onCreate method called when the activity is first created.
     * 
     * @param savedInstanceState Bundle
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.manual);
    }

    /**
     * onDialogButtonClick Method.
     * 
     * @param v
     */
    public void onDialogButtonClick(View v) {
        showDialog(MY_SIMPLE_DIALOG_ID);
    }

    /**
     * Method onMovingDateDialogButtonClick.
     * 
     * @param v
     */
    public void onMovingDateDialogButtonClick(View v) {
        showDialog(moving);
    }

    /**
     * onSetUpSerwerDialogButtonClick.
     * 
     * @param v
     */
    public void onSetUpSerwerDialogButtonClick(View v) {
        showDialog(set_up_server);
    }

    /**
     * onConnectToServerDialogButtonClick.
     * 
     * @param v
     */
    public void onConnectToServerDialogButtonClick(View v) {
        showDialog(connect_to_server);
    }

    /**
     * onAddUserDialogButtonClick.
     * 
     * @param v
     */
    public void onAddUserDialogButtonClick(View v) {
        showDialog(add_user);
    }

    /**
     * onDeleteUserDialogButtonClick.
     * 
     * @param v
     */
    public void onDeleteUserDialogButtonClick(View v) {
        showDialog(delete_user);
    }

    /**
     * onModifyUserDialogButtonClick.
     * 
     * @param v
     */
    public void onModifyUserDialogButtonClick(View v) {
        showDialog(modify_user);
    }

    /**
     * onCheckStatsDialogButtonClick.
     * 
     * @param v
     */
    public void onCheckStatsDialogButtonClick(View v) {
        showDialog(check_stats);
    }

    /**
     * onWwwDialogButtonClick.
     * 
     * @param v
     */
    public void onWwwDialogButtonClick(View v) {
        showDialog(www);
    }

    /**
     * onExitDialogButtonClick.
     * 
     * @param v
     */
    public void onExitDialogButtonClick(View v) {
        showDialog(exit);
    }

    /**
     * onDateDialogButtonClick.
     * 
     * @param v
     */
    public void onDateDialogButtonClick(View v) {
        Intent mainIntent = new Intent().setClass(ManualActivity.this,
                FormActivity.class);
        startActivity(mainIntent);
        // showDialog(MY_DATE_DIALOG_ID);
    }

    /**
     * onCreateDialog.
     * 
     * @param id
     * @return 
     */
    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
        case moving:
            AlertDialog.Builder builder3 = new AlertDialog.Builder(this);
            builder3.setTitle(R.string.moving);
            builder3.setMessage(R.string.moving_msg);
            builder3.setIcon(android.R.drawable.btn_star);
            builder3.setPositiveButton(android.R.string.ok,
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            return;
                        }
                    });
            return builder3.create();

        case set_up_server:
            AlertDialog.Builder builder4 = new AlertDialog.Builder(this);
            builder4.setTitle(R.string.set_up_server);
            builder4.setMessage(R.string.set_up_server_msg);
            builder4.setIcon(android.R.drawable.btn_star);
            builder4.setPositiveButton(android.R.string.ok,
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            return;
                        }
                    });
            return builder4.create();

        case connect_to_server:
            AlertDialog.Builder builder5 = new AlertDialog.Builder(this);
            builder5.setTitle(R.string.connect_to_server);
            builder5.setMessage(R.string.connect_to_server_msg);
            builder5.setIcon(android.R.drawable.btn_star);
            builder5.setPositiveButton(android.R.string.ok,
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            return;
                        }
                    });
            return builder5.create();

        case add_user:
            AlertDialog.Builder builder6 = new AlertDialog.Builder(this);
            builder6.setTitle(R.string.add_user);
            builder6.setMessage(R.string.add_user_msg);
            builder6.setIcon(android.R.drawable.btn_star);
            builder6.setPositiveButton(android.R.string.ok,
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            return;
                        }
                    });
            return builder6.create();

        case delete_user:
            AlertDialog.Builder builder7 = new AlertDialog.Builder(this);
            builder7.setTitle(R.string.delete_user);
            builder7.setMessage(R.string.delete_user_msg);
            builder7.setIcon(android.R.drawable.btn_star);
            builder7.setPositiveButton(android.R.string.ok,
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            return;
                        }
                    });
            return builder7.create();

        case modify_user:
            AlertDialog.Builder builder8 = new AlertDialog.Builder(this);
            builder8.setTitle(R.string.modify_user);
            builder8.setMessage(R.string.modify_user_msg);
            builder8.setIcon(android.R.drawable.btn_star);
            builder8.setPositiveButton(android.R.string.ok,
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            return;
                        }
                    });
            return builder8.create();

        case check_stats:
            AlertDialog.Builder builder9 = new AlertDialog.Builder(this);
            builder9.setTitle(R.string.stats_check);
            builder9.setMessage(R.string.stats_check_msg);
            builder9.setIcon(android.R.drawable.btn_star);
            builder9.setPositiveButton(android.R.string.ok,
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            return;
                        }
                    });
            return builder9.create();

        case www:
            AlertDialog.Builder builder10 = new AlertDialog.Builder(this);
            builder10.setTitle(R.string.www);
            builder10.setMessage(R.string.www_msg);
            builder10.setIcon(android.R.drawable.btn_star);
            builder10.setPositiveButton(android.R.string.ok,
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            return;
                        }
                    });
            return builder10.create();

        case exit:
            AlertDialog.Builder builder11 = new AlertDialog.Builder(this);
            builder11.setTitle(R.string.exit);
            builder11.setMessage(R.string.exit_msg);
            builder11.setIcon(android.R.drawable.btn_star);
            builder11.setPositiveButton(android.R.string.ok,
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            return;
                        }
                    });
            return builder11.create();

        }
        return null;

    }

    /**
     * onPrepareDialog.
     * @param id
     * @param dialog
     * @return
     */
    @Override
    protected void onPrepareDialog(int id, Dialog dialog) {
        super.onPrepareDialog(id, dialog);
        switch (id) {
        case MY_SUPER_SIMPLE_DIALOG_ID:
            // Static dialog contents. No initialization needed
            break;
        case MY_SIMPLE_DIALOG_ID:
            // Static dialog contents. No initialization needed
            break;
        case MY_DIALOG_ID:
            // Some initialization needed. Date/time changes each time this
            // dialog is displayed, so update its contents in prepare (not
            // create)
            AlertDialog myDialog = (AlertDialog) dialog;
            SimpleDateFormat dFormat = new SimpleDateFormat(
                    "yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            myDialog.setMessage("This dialog was launched at "
                    + dFormat.format(mCurrentTime));
            break;
        }
        return;
    }

}