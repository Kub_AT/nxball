package com.nxball.game;


/**
 * Player class.
 * @version 0.0.1
 *
 */
public class Player {

	/**
	 * Player nickname.
	 */
	private String name;
	/**
	 * X pos.
	 */
	private float x;
	/**
	 * Y pos.
	 */
	private float y;
	
	/**
	 * true - player is controlled from this device.
	 */
	private Boolean local;

	/**
	 * Method set nickname.
	 * @param nickname Player Nickname
	 */
	public final void setName(final String nickname) {
		this.name = nickname;
	}

	/**
	 * Get Nickname.
	 * @return nickname
	 */
	public final String getName() {
		return this.name;
	}

	/**
	 * Player.
	 * @param nickname Player Nickname
	 * @param loc      Is player controller locally
	 */
	public Player(final String nickname, final Boolean loc) {
		this.name = nickname;
		this.local = loc;
	}
	
	/**
	 * Getter for x.
	 * 
	 * @return int
	 */
	public final float getX() {
		return this.x;
	}
	
	/**
	 * Getter for x.
	 * 
	 * @return int
	 */
	public final float getY() {
		return this.y;
	}
	
	/**
	 * Setter for local.
	 * 
	 * @param xx Position x
	 */
	public final void setX(final float xx) {
		this.x = xx;
	}
	
	/**
	 * Setter for y.
	 * 
	 * @param yy Positon y
	 */
	public final void setY(final float yy) {
		this.y = yy;
	}
	
	/**
	 * Getter for local.
	 * 
	 * @return Boolean
	 */
	public final Boolean isLocal() {
		return this.local;
	}
	
	/**
	 * Setter for local.
	 * 
	 * @param loc Local 
	 */
	public final void setLocal(final Boolean loc) {
		this.local = loc;
	}
	
	/**
	 * Move method.
	 * 
	 * @param xAxis
	 * @param yAxis
	 */
	public final void move(final float xAxis, final float yAxis) {
		this.x += xAxis;
		this.y += yAxis;
	}

}
