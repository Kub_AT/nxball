package com.nxball.game.test.Unit;

import junit.framework.TestCase;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import static org.junit.Assert.*;

import com.nxball.game.Player;
import com.nxball.game.PlayerManager;

/**
 * PlayerManager tests.
 * 
 * @author Michal
 * 
 */
public class PlayerManagerTest {

    @Rule
    public ExpectedException exception = ExpectedException.none();

    /**
     * Test constructor method.
     * 
     * @test
     */
    @Test
    public final void testConstructor() {
        PlayerManager manager = new PlayerManager();
        assertEquals(PlayerManager.class, manager.getClass());
    }

    /**
     * Test createPlayer method.
     * 
     * @test
     */
    @Test
    public final void testCreatePlayerOk() {
        PlayerManager manager = new PlayerManager();
        assertEquals(Player.class, manager.createPlayer("Gracz", true)
                .getClass());
    }

    /**
     * Test createPlayer method.
     */
    @Test
    public final void testCreatePlayerEmptyName() {
        exception.expect(IllegalArgumentException.class);
        PlayerManager manager = new PlayerManager();
        manager.createPlayer("", true);
    }

    /**
     * Test createPlayer method.
     */
    @Test
    public final void testCreatePlayerSecondLocalPlayer() {
        exception.expect(RuntimeException.class);
        PlayerManager manager = new PlayerManager();
        manager.createPlayer("blabla", true);
        manager.createPlayer("blabla2", true);
    }

    /**
     * Test createPlayer method.
     */
    @Test
    public final void testGetByNameExistingNamePassed() {
        PlayerManager manager = new PlayerManager();
        manager.createPlayer("Playername", true);
        Player player = manager.getByName("Playername");
        assertEquals("Playername", player.getName());
    }

}
