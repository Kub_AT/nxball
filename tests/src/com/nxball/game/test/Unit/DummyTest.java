package com.nxball.game.test.Unit;

import android.test.AndroidTestCase;
import junit.framework.Assert; 

/**
 * DummyTest Class.
 *
 */
public class DummyTest extends AndroidTestCase {
	
    /**
     * testSomething.
     * @throws Throwable
     */
	public void testSomething() throws Throwable {  
	       Assert.assertTrue(1 + 1 == 2);  
	}  
	  
	/**
	 * testSomethingElse.
	 * @throws Throwable
	 */
	public void testSomethingElse() throws Throwable {  
	       Assert.assertTrue(1 + 3 == 4);  
	} 

}
