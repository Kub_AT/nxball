package com.nxball.game.test.Unit;

import android.test.AndroidTestCase;
import junit.framework.Assert;
import com.nxball.game.*;

/**
 * Player tests.
 * 
 * @author Michal
 * 
 */
public class PlayerTest extends AndroidTestCase {

    /**
     * Test player method.
     */
    public final void testPlayer() {
        Player player = new Player("Player", true);
        Assert.assertEquals("Player", player.getName());
    }

}
